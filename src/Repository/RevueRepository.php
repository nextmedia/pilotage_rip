<?php

namespace App\Repository;

use App\Entity\Revue;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Revue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Revue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Revue[]    findAll()
 * @method Revue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RevueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Revue::class);
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function findLastReviewUser(User $user)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('revue')
            ->from(Revue::class, 'revue')
            ->leftJoin("revue.createdFor", "user")
            ->where("user.id = :userId")
            ->orderBy('revue.createdAt', 'DESC')
            ->setParameter("userId", $user->getId())
        ;

        $result = $qb->getQuery()->getResult();
        return !empty($result[0]) ? $result[0] : null;
    }
}
