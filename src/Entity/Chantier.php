<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChantierRepository")
 * @ORM\Table(name="new_chantier")
 */
class Chantier
{
    CONST STATE_ANNULE = "annule";
    CONST STATE_CLOTURE = "cloture";
    CONST STATE_REFUSE = "refuse";

    CONST STATES = [
      self::STATE_ANNULE => "Annule",
      self::STATE_CLOTURE => "Cloturé",
      self::STATE_REFUSE => "Refusé"
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $codeAffaire;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $etat;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nomDemandeur;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $entiteDemandeur;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $commentaireCinkeP;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $rue;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $complement;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $intitule;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prepaTheory;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prepaRetenue;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prepaRealisee;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priorite;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDebutReal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDebutPrepa;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFinPrepa;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFinReal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateCloture;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nomCA;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="CommentaireChantier", mappedBy="chantier", cascade={"remove"})
     */
    private $commentaires;

    /**
     * @ORM\ManyToOne(targetEntity="Commune", inversedBy="chantiers")
     */
    private $commune;

    /**
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="chantiers")
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity="TypeTravail", inversedBy="chantiers")
     */
    private $typeTravail;

    /**
     * @ORM\ManyToOne(targetEntity="BO", inversedBy="prepas")
     */
    private $entitePrepa;

    /**
     * @ORM\ManyToOne(targetEntity="BO", inversedBy="reals")
     */
    private $entiteReal;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="chantiersResponsable")
     */
    private $responsable;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="chantiersPreparateur")
     */
    private $preparateur;

    /**
     * @ORM\ManyToMany(targetEntity="Revue", mappedBy="chantiers")
     */
    private $revues;

    /**
     * Chantier constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->commentaires = new ArrayCollection();
        $this->revues = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getCodeAffaire()
    {
        return $this->codeAffaire;
    }

    /**
     * @param mixed $codeAffaire
     */
    public function setCodeAffaire($codeAffaire)
    {
        $this->codeAffaire = $codeAffaire;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getNomDemandeur()
    {
        return $this->nomDemandeur;
    }

    /**
     * @param mixed $nomDemandeur
     */
    public function setNomDemandeur($nomDemandeur)
    {
        $this->nomDemandeur = $nomDemandeur;
    }

    /**
     * @return mixed
     */
    public function getEntiteDemandeur()
    {
        return $this->entiteDemandeur;
    }

    /**
     * @param mixed $entiteDemandeur
     */
    public function setEntiteDemandeur($entiteDemandeur)
    {
        $this->entiteDemandeur = $entiteDemandeur;
    }

    /**
     * @return mixed
     */
    public function getCommentaireCinkeP()
    {
        return $this->commentaireCinkeP;
    }

    /**
     * @param mixed $commentaireCinkeP
     */
    public function setCommentaireCinkeP($commentaireCinkeP)
    {
        $this->commentaireCinkeP = $commentaireCinkeP;
    }

    /**
     * @return mixed
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * @param mixed $rue
     */
    public function setRue($rue)
    {
        $this->rue = $rue;
    }

    /**
     * @return mixed
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * @param mixed $complement
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;
    }

    /**
     * @return mixed
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param mixed $intitule
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
    }

    /**
     * @return mixed
     */
    public function getPrepaTheory()
    {
        return $this->prepaTheory;
    }

    /**
     * @param mixed $prepaTheory
     */
    public function setPrepaTheory($prepaTheory)
    {
        $this->prepaTheory = $prepaTheory;
    }

    /**
     * @return mixed
     */
    public function getPrepaRetenue()
    {
        return $this->prepaRetenue;
    }

    /**
     * @param mixed $prepaRetenue
     */
    public function setPrepaRetenue($prepaRetenue)
    {
        $this->prepaRetenue = $prepaRetenue;
    }

    /**
     * @return mixed
     */
    public function getPrepaRealisee()
    {
        return $this->prepaRealisee;
    }

    /**
     * @param mixed $prepaRealisee
     */
    public function setPrepaRealisee($prepaRealisee)
    {
        $this->prepaRealisee = $prepaRealisee;
    }

    /**
     * @return mixed
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    /**
     * @param mixed $priorite
     */
    public function setPriorite($priorite)
    {
        $this->priorite = $priorite;
    }

    /**
     * @return mixed
     */
    public function getDateDebutReal()
    {
        return $this->dateDebutReal;
    }

    /**
     * @param mixed $dateDebutReal
     */
    public function setDateDebutReal($dateDebutReal)
    {
        // Défini l'heure à 00:00
        if (!empty($dateDebutReal)) $dateDebutReal->setTime(0,0);
        $this->dateDebutReal = $dateDebutReal;
    }

    /**
     * @return mixed
     */
    public function getDateDebutPrepa()
    {
        return $this->dateDebutPrepa;
    }

    /**
     * @param mixed $dateDebutPrepa
     */
    public function setDateDebutPrepa($dateDebutPrepa)
    {
        // Défini l'heure à 00:00
        if (!empty($dateDebutPrepa)) $dateDebutPrepa->setTime(0,0);
        $this->dateDebutPrepa = $dateDebutPrepa;
    }

    /**
     * @return mixed
     */
    public function getDateFinPrepa()
    {
        return $this->dateFinPrepa;
    }

    /**
     * @param mixed $dateFinPrepa
     */
    public function setDateFinPrepa($dateFinPrepa)
    {
        // Défini l'heure à 23:59
        if (!empty($dateFinPrepa)) $dateFinPrepa->setTime(23,59);
        $this->dateFinPrepa = $dateFinPrepa;
    }

    /**
     * @return mixed
     */
    public function getDateFinReal()
    {
        return $this->dateFinReal;
    }

    /**
     * @param mixed $dateFinReal
     */
    public function setDateFinReal($dateFinReal)
    {
        // Défini l'heure à 23:59
        if (!empty($dateFinReal)) $dateFinReal->setTime(23,59);
        $this->dateFinReal = $dateFinReal;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getDateCloture()
    {
        return $this->dateCloture;
    }

    /**
     * @param mixed $dateCloture
     */
    public function setDateCloture($dateCloture)
    {
        $this->dateCloture = $dateCloture;
    }

    /**
     * @return mixed
     */
    public function getNomCA()
    {
        return $this->nomCA;
    }

    /**
     * @param mixed $nomCA
     */
    public function setNomCA($nomCA)
    {
        $this->nomCA = $nomCA;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * @param CommentaireChantier $commentaire
     */
    public function addCommentaire(CommentaireChantier $commentaire)
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires->add($commentaire);
        }
    }

    /**
     * @param CommentaireChantier $commentaire
     */
    public function removeCommentaire(CommentaireChantier $commentaire)
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
        }
    }

    /**
     * @return mixed
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * @param mixed $commune
     */
    public function setCommune($commune)
    {
        $this->commune = $commune;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function getTypeTravail()
    {
        return $this->typeTravail;
    }

    /**
     * @param mixed $typeTravail
     */
    public function setTypeTravail($typeTravail)
    {
        $this->typeTravail = $typeTravail;
    }

    /**
     * @return mixed
     */
    public function getEntitePrepa()
    {
        return $this->entitePrepa;
    }

    /**
     * @param mixed $entitePrepa
     */
    public function setEntitePrepa($entitePrepa)
    {
        $this->entitePrepa = $entitePrepa;
    }

    /**
     * @return mixed
     */
    public function getEntiteReal()
    {
        return $this->entiteReal;
    }

    /**
     * @param mixed $entiteReal
     */
    public function setEntiteReal($entiteReal)
    {
        $this->entiteReal = $entiteReal;
    }

    /**
     * @return mixed
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * @param mixed $responsable
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;
    }

    /**
     * @return mixed
     */
    public function getPreparateur()
    {
        return $this->preparateur;
    }

    /**
     * @param mixed $preparateur
     */
    public function setPreparateur($preparateur)
    {
        $this->preparateur = $preparateur;
    }

    /**
     * @return ArrayCollection
     */
    public function getRevues()
    {
        return $this->revues;
    }

    /**
     * @param ArrayCollection $revues
     */
    public function setRevues(ArrayCollection $revues)
    {
        $this->revues = $revues;
    }


    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->number;
    }

	/**
	 * @return mixed
	 */
    public function getTempsRestant(){
    	if( !empty($this->getPrepaRetenue()) && !empty($this->getPrepaRealisee()) ){
    		return ($this->getPrepaRetenue() - $this->getPrepaRealisee());
		}else{
    		return $this->getPrepaRetenue();
		}
	}
}
