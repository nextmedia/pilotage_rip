<?php

namespace App\Controller;

use App\Entity\Activite;
use App\Repository\ActiviteRepository;
use App\Service\UtilsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ActiviteController
 * @package App\Controller
 * @Route("/activite", name="activity_")
 */
class ActiviteController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param ActiviteRepository $activiteRepository
     * @return Response
     */
    public function main(ActiviteRepository $activiteRepository)
    {
        $activites = $activiteRepository->findAll();

        return $this->render("activite/main.html.twig", [
            "activites" => $activites
        ]);
    }

    /**
     * @Route("/changePrepa/{id}", name="change_prepa", options={"expose"=true})
     * @param Request $request
     * @param Activite $activite
     * @param EntityManagerInterface $em
     * @param UtilsService $utilsService
     * @return JsonResponse
     */
    public function changePrepa(
        Request $request,
        Activite $activite,
        EntityManagerInterface $em,
        UtilsService $utilsService
    ) {
        $checked = $request->get("checked");
        if ($checked === "true")
        {
            $activite->setIsPrepa(true);
        } else {
            $activite->setIsPrepa(false);
        }

        $em->flush();
        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);

        return new JsonResponse($response);
    }
}