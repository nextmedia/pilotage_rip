const routes = require('./routing/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

export {onSwitchButton};

$(function() {
    onSwitchButton();
});

/**
 * Au click sur un bouton de switch
 */
function onSwitchButton()
{
    var $switch = $(".switch");

    $switch.off("change").on("change", function() {
       var that = $(this),
           id = that.data("id"),
           checked = that.find("input").is(":checked");

       $.ajax({
           url : Routing.generate("activity_change_prepa",{"id" : id, "checked" : checked}),
           type : "POST",
           dataType : "json",
           success : function(data){
               console.log(data);
           }
       });
    });
}