<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Activite;
use App\Entity\CommentaireRevue;
use App\Entity\Planning;
use App\Entity\Revue;
use App\Repository\ActiviteRepository;
use App\Repository\ChantierRepository;
use App\Repository\PlanningRepository;
use DateTime;
use App\Entity\User;
use App\Entity\Chantier;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var UserRepository $userRepository */
    private $userRepository;
    /** @var Security $security */
    private $security;
    /** @var ActionService $actionService */
    private $actionService;
    /** @var RevueService $revueService */
    private $revueService;
    /** @var KernelInterface $kernel */
    private $kernel;

    /**
     * AgenceService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param UserRepository $userRepository
     * @param Security $security
     * @param ActionService $actionService
     * @param RevueService $revueService
     * @param KernelInterface $kernel
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        Security $security,
        ActionService $actionService,
		RevueService  $revueService,
        KernelInterface $kernel
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->userRepository = $userRepository;
        $this->security = $security;
        $this->actionService = $actionService;
        $this->revueService = $revueService;
        $this->kernel = $kernel;
    }

    /**
     * @param User $user
     */
    public function edit(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_USER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_USER]["edit"],
            $user
        );
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getSites(User $user)
    {
        /** @var ChantierRepository $chantierRepository */
        $chantierRepository = $this->em->getRepository(Chantier::class);
        return $chantierRepository->findByPreparateur($user);
    }

    /**
     * @param $dr
     * @param $agence
     * @param $bo
     * @param $role
     * @return int|mixed|string
     */
    public function getUsersByFilter($dr, $agence, $bo, $role = null)
    {
        return $this->userRepository->findByFilter($dr, $agence, $bo, $role);
    }

    /**
     * @param User $user
     * @param $message
     * @throws Exception
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function closeReview(User $user, $message)
    {
        $revue = new Revue();

        if (!empty($message)) {
            $commentaire = new CommentaireRevue();
            $commentaire->setMessage($message);
            $commentaire->setUser($this->security->getUser());

            $revue->setCommentaire($commentaire);
            $commentaire->setRevue($revue);

            $this->em->persist($commentaire);
        }

        $user->setDateLastReview(new DateTime());
        $revue->setCreatedFor($user);
        $revue->setCreatedBy($this->security->getUser());

        /** @var ChantierRepository $chantierRepo */
        $chantierRepo = $this->em->getRepository(Chantier::class);
        $chantiers = $chantierRepo->findReview($user);
        $revue->setChantiers($chantiers);

		// Appel au service RevueService pour génération du document + envoi
		$file = $this->revueService->createRapportRevue($chantiers);
		$this->revueService->sendRevue( $revue, $file );

        $this->em->persist($revue);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_REVUE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_REVUE]["close"],
            $user
        );
    }

    /**
     * @param User $user
     */
    public function delete(User $user)
    {
        $this->actionService->newAction(
            Action::TYPE_USER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_USER]["remove"],
            $user
        );

        $this->em->remove($user);
        $this->em->flush();
    }

    /**
     * @param User $user
     */
    public function init(User $user)
    {
        $user->addRole(User::ROLE_RIP);

        if (!empty($user->getFirstname())) {
            $firstName = ucfirst(strtolower($user->getFirstname()));
            $user->setFirstname($firstName);
        }
        if (!empty($user->getLastname())) {
            $lastname = strtoupper($user->getLastname());
            $user->setLastname($lastname);
        }

        $this->em->persist($user);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_USER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_USER]["create"],
            $user
        );
    }

    /**
     * @param User $user
     * @param bool $status
     * @return int|mixed|string
     */
    public function getChantiersByStatus(User $user, bool $status)
    {
        /** @var ChantierRepository $chantierRepository */
        $chantierRepository = $this->em->getRepository(Chantier::class);

        return $chantierRepository->findByStatus($user, $status);
    }

    /**
     * @param $fichier
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \Exception
     */
    public function importPlanning($fichier)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        /** @var ActiviteRepository $activiteRepository */
        $activiteRepository = $this->em->getRepository(Activite::class);
        /** @var PlanningRepository $planningRepository */
        $planningRepository = $this->em->getRepository(Planning::class);

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');
        $reader->setReadDataOnly(TRUE);

        $file = file_get_contents($fichier->getRealPath());
        $utf8_file = utf8_encode($file);
        $new_file = $this->kernel->getProjectDir()."/uploads/tmp/planningImport.csv";
        file_put_contents($new_file , $utf8_file);

        $spreadsheet = $reader->load($new_file);
        $worksheet = $spreadsheet->getActiveSheet();

        foreach ($worksheet->getRowIterator() as $row) {
            if ($row->getRowIndex() === 1) continue;
            $flush = false;

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $arrayLine = [];
            foreach ($cellIterator as $cell) {
                $arrayLine[$cell->getColumn()] = $cell->getValue();
            }

            /** @var User $user */
            $user = $this->userRepository->findOneBy(["nni" => $arrayLine["E"]]);
            if (empty($user)) {
                $name = explode(" ", $arrayLine["F"]);
                $lastname = str_replace("ë", "e", array_shift($name));
                $firstName = str_replace("ë", "e", implode(" ", $name));

                $user = $this->em->getRepository(User::class)->findOneByIdentity($firstName, $lastname);
                if (empty($user)) {
                    $user = new User();
                    $user->setLastname($lastname);
                    $user->setFirstname($firstName);
                    $this->init($user);
                }
                $user->setNni($arrayLine["E"]);
            }

            /** @var Activite $activite */
            $activite = $activiteRepository->findOneBy(["libelle" => $arrayLine["J"]]);
            if (empty($activite)) {
                $activite = new Activite();
                $activite->setLibelle($arrayLine["J"]);
                $this->em->persist($activite);
                $flush = true;
            }

            $start = date_create_from_format("Y-d-m H:i:s", $arrayLine["G"] . " " . $arrayLine["H"]);
            $end = date_create_from_format("Y-d-m H:i:s", $arrayLine["G"] . " " . $arrayLine["I"]);
            if (empty($start) || empty($end)) {
                $start = date_create_from_format("d/m/Y H:i:s", $arrayLine["G"] . " " . $arrayLine["H"]);
                $end = date_create_from_format("d/m/Y H:i:s", $arrayLine["G"] . " " . $arrayLine["I"]);
            }

            if (empty($planningRepository->findExistantPlanning($user, $start, $end))) {
                $planning = new Planning();
                $planning->setUser($user);
                $planning->setActivite($activite);
                $planning->setStart($start);
                $planning->setEnd($end);

                $this->em->persist($planning);
            }
            if ($flush)
                $this->em->flush();
        }
        $this->em->flush();
        unlink($new_file);
    }

    /**
     * Retourne un resultat en heures
     * @param User $user
     * @param $start
     * @param $end
     * @return int|mixed|string
     */
    public function getTimePrepa(User $user, $start, $end)
    {
        /** @var PlanningRepository $planningRepository */
        $planningRepository = $this->em->getRepository(Planning::class);

        $plannings = $planningRepository->findByUserAndDate($user, $start, $end, true);

        $hours = 0;
        /** @var Planning $planning */
        foreach($plannings as $planning) {
            $start = $planning->getStart()->format("H") * 60 + $planning->getStart()->format("i");
            $end = $planning->getEnd()->format("H") * 60 + $planning->getEnd()->format("i");

            $hours += $end - $start;
        }

        return round($hours / 60, 1);
    }
}