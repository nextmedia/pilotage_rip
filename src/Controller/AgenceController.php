<?php

namespace App\Controller;

use App\Repository\BORepository;
use LogicException;
use App\Entity\Agence;
use App\Form\AgenceType;
use App\Service\AgenceService;
use App\Repository\AgenceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AgenceController
 * @package App\Controller
 * @Route("/agence", name="agency_")
 */
class AgenceController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param AgenceRepository $agenceRepository
     * @return Response
     */
    public function main(AgenceRepository $agenceRepository)
    {
        $agences = $agenceRepository->findAll();

        return $this->render("agence/main.html.twig", [
            "agences" => $agences
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param AgenceService $agenceService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        AgenceService $agenceService,
        TranslatorInterface $translator
    ) {
        $agence = new Agence();

        $agenceForm = $this->createForm(AgenceType::class, $agence);
        $agenceForm->handleRequest($request);

        if ($agenceForm->isSubmitted() && $agenceForm->isValid()) {
            try {
                $agenceService->init($agence);
                $this->addFlash("success", $translator->trans("agency.sentence.new_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
                return $this->redirectToRoute("agency_new");
            }

            return $this->redirectToRoute("agency_info", ["id" => $agence->getId()]);
        }

        return $this->render("agence/info.html.twig", [
            "agenceForm" => $agenceForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param Agence $agence
     * @param Request $request
     * @param AgenceService $agenceService
     * @param BORepository $BORepository
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function info(
        Agence $agence,
        Request $request,
        AgenceService $agenceService,
        BORepository $BORepository,
        TranslatorInterface $translator
    ) {
        $agenceForm = $this->createForm(AgenceType::class, $agence);
        $agenceForm->handleRequest($request);

        if ($agenceForm->isSubmitted() && $agenceForm->isValid()) {
            try {
                $agenceService->edit($agence);
                $this->addFlash("success", $translator->trans("utils.edit_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirectToRoute("agency_info", ["id" => $agence->getId()]);
        }

        $listBo = $BORepository->findBy(["agence" => $agence->getId()]);

        return $this->render("agence/info.html.twig", [
            "agence" => $agence,
            "listBo" => $listBo,
            "agenceForm" => $agenceForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param Agence $agence
     * @param AgenceService $agenceService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        Agence $agence,
        AgenceService $agenceService,
        TranslatorInterface $translator
    ) {
        $agenceService->delete($agence);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("agency_main");
    }
}