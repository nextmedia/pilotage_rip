<?php

namespace App\Form;

use App\Entity\Agence;
use App\Entity\DR;
use App\Repository\AgenceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DRType extends AbstractType
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * CommuneType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dr = $options["data"];

        $builder
            ->add('name', TextType::class, [
                "label" => $this->translator->trans("dr.libelle"),
                "required" => true,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('agences', EntityType::class, [
                "label" => $this->translator->trans("agency.libelle", ["%count%" => 2]),
                "class" => Agence::class,
                "query_builder" => function (AgenceRepository $agenceRepository) use ($dr) {
                    return $agenceRepository->findAvailablesAgence($dr);
                },
                "choice_label" => "name",
                "required" => false,
                "multiple" => true,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control select2"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => $this->translator->trans("utils.validate"),
                "attr" => [
                    "class" => "btn btn-success btn-sm"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DR::class,
        ]);
    }
}
