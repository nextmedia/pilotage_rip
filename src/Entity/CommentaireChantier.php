<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireChantierRepository")
 * @ORM\Table(name="new_commentaire_chantier")
 */
class CommentaireChantier extends Commentaire
{
    /**
     * @ORM\ManyToOne(targetEntity="Chantier", inversedBy="commentaires")
     */
    private $chantier;

    /**
     * @return mixed
     */
    public function getChantier()
    {
        return $this->chantier;
    }

    /**
     * @param mixed $chantier
     */
    public function setChantier(Chantier $chantier)
    {
        $this->chantier = $chantier;
    }
}
