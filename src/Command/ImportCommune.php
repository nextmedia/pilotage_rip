<?php


namespace App\Command;

use App\Entity\Agence;
use App\Entity\BO;
use App\Entity\Commune;
use App\Entity\DR;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommune extends Command
{
    protected static $defaultName = 'import:commune';

    /** @var EntityManagerInterface $em */
    private $em;
    /** @var KernelInterface $kernel */
    private $kernel;

    /**
     * ImportSuiviSociety constructor.
     * @param EntityManagerInterface $em
     * @param KernelInterface $kernel
     */
    public function __construct(EntityManagerInterface $em, KernelInterface $kernel)
    {
        $this->em = $em;
        $this->kernel = $kernel;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Import des communes')

            ->addArgument('dr', InputArgument::REQUIRED, 'DR concernée ?')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Cette commande va créer en base toutes les communes/bo/agences à partir du fichier excel.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Import en cours ...");

        try {
            $this->import($input);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return -1;
        }

        $output->writeln("Terminé !");

        return 0;
    }

    /**
     * @param InputInterface $input
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function import(InputInterface $input)
    {
        $path = $this->kernel->getProjectDir() . "/uploads";

        $drName = $input->getArgument("dr");
        $fileName = "/communes_".$drName.".xlsx";

        if (!file_exists($path.$fileName)){
            throw new \PhpOffice\PhpSpreadsheet\Exception($path.$fileName);
        }

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);

        $spreadsheet = $reader->load($path.$fileName);

        $worksheet = $spreadsheet->getActiveSheet();

        $communeRepository = $this->em->getRepository(Commune::class);
        $boRepository = $this->em->getRepository(BO::class);
        $agenceRepository = $this->em->getRepository(Agence::class);
        $drRepository = $this->em->getRepository(DR::class);

        $dr = $drRepository->findOneBy(["name" => $drName]);
        if (empty($dr)) {
            $dr = new DR();
            $dr->setName($drName);
            $this->em->persist($dr);
            $this->em->flush();
        }

        foreach ($worksheet->getRowIterator() as $row) {
            if($row->getRowIndex() === 1) continue;

            $flush = false;

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);

            $arrayLine = [];
            foreach ($cellIterator as $cell) {
                $arrayLine[$cell->getColumn()] = $cell->getValue();
            }

            if (empty($communeRepository->findOneBy(["insee" => $arrayLine["A"]]))) {
                $commune = new Commune();
                $commune->setInsee($arrayLine["A"]);
                $commune->setLibelle($arrayLine["B"]);
                $this->em->persist($commune);

                $bo = $boRepository->findOneBy(["name" => $arrayLine["C"]]);
                if (empty($bo)) {
                    $bo = new BO();
                    $bo->setName($arrayLine["C"]);
                    $this->em->persist($bo);
                    $flush = true;
                }

                $agence = $agenceRepository->findOneBy(["name" => $arrayLine["D"]]);
                if (empty($agence)) {
                    $agence = new Agence();
                    $agence->setName($arrayLine["D"]);
                    $this->em->persist($agence);
                    $flush = true;
                }

                $commune->addBo($bo);
                $bo->addCommune($commune);
                $bo->setAgence($agence);
                $agence->setDr($dr);

                if ($flush) { $this->em->flush(); }
            }
        }
        $this->em->flush();
    }
}