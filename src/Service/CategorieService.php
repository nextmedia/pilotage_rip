<?php

namespace App\Service;

use App\Entity\Action;
use LogicException;
use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class CategorieService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var CategorieRepository $categorieRepository */
    private $categorieRepository;
    /** @var Security $security */
    private $security;
    /** @var ActionService $actionService */
    private $actionService;

    /**
     * AgenceService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param CategorieRepository $categorieRepository
     * @param Security $security
     * @param ActionService $actionService
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        CategorieRepository $categorieRepository,
        Security $security,
        ActionService $actionService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->categorieRepository = $categorieRepository;
        $this->security = $security;
        $this->actionService = $actionService;
    }

    /**
     * @param Categorie $categorie
     */
    public function init(Categorie $categorie)
    {
        $searchCategorie = $this->categorieRepository->findOneBy(["libelle" => $categorie->getLibelle()]);
        if (!empty($searchCategorie)) {
            throw new LogicException($this->translator->trans("error.category.libelle_exist"));
        }

        $this->em->persist($categorie);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_CATEGORIE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CATEGORIE]["create"],
            $categorie
        );
    }

    /**
     * @param Categorie $categorie
     */
    public function edit(Categorie $categorie)
    {
        $searchCategorie = $this->categorieRepository->findOneBy(["libelle" => $categorie->getLibelle()]);
        if (!empty($searchCategorie) && $categorie->getId() != $searchCategorie->getId()) {
            throw new LogicException($this->translator->trans("error.category.libelle_exist"));
        }

        $this->em->persist($categorie);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_CATEGORIE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CATEGORIE]["edit"],
            $categorie
        );
    }

    /**
     * @param Categorie $categorie
     */
    public function delete(Categorie $categorie)
    {
        $this->actionService->newAction(
            Action::TYPE_CATEGORIE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CATEGORIE]["remove"],
            $categorie
        );

        $this->em->remove($categorie);
        $this->em->flush();
    }
}