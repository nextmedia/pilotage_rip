<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BORepository")
 * @ORM\Table(name="new_bo")
 */
class BO
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Chantier", mappedBy="entitePrepa")
     */
    private $prepas;

    /**
     * @ORM\OneToMany(targetEntity="Chantier", mappedBy="entiteReal")
     */
    private $reals;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="bo")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="Agence", inversedBy="listBo")
     */
    private $agence;

    /**
     * @ORM\ManyToMany(targetEntity="Commune", mappedBy="listBo")
     */
    private $communes;

    /**
     * BO constructor.
     */
    public function __construct()
    {
        $this->prepas = new ArrayCollection();
        $this->reals = new ArrayCollection();
        $this->communes = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPrepas()
    {
        return $this->prepas;
    }

    /**
     * @param ArrayCollection $prepas
     */
    public function setPrepas(ArrayCollection $prepas)
    {
        $this->prepas = $prepas;
    }

    /**
     * @param Chantier $chantier
     */
    public function addPrepa(Chantier $chantier)
    {
        $this->prepas->add($chantier);
    }

    /**
     * @param Chantier $chantier
     */
    public function removePrepa(Chantier $chantier)
    {
        $this->prepas->removeElement($chantier);
    }

    /**
     * @param Chantier $chantier
     */
    public function addReal(Chantier $chantier)
    {
        $this->reals->add($chantier);
    }

    /**
     * @param Chantier $chantier
     */
    public function removeReal(Chantier $chantier)
    {
        $this->reals->removeElement($chantier);
    }

    /**
     * @return ArrayCollection
     */
    public function getReals()
    {
        return $this->reals;
    }

    /**
     * @param ArrayCollection $reals
     */
    public function setReals(ArrayCollection $reals): void
    {
        $this->reals = $reals;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers(ArrayCollection $users): void
    {
        $this->users = $users;
    }

    /**
     * @param User $user
     */
    public function adduser(User $user)
    {
        $this->users->add($user);
    }

    /**
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getAgence()
    {
        return $this->agence;
    }

    /**
     * @param mixed $agence
     */
    public function setAgence($agence)
    {
        $this->agence = $agence;
    }

    /**
     * Supprimer le lien entre une agence et un BO
     */
    public function removeAgence()
    {
        if (!empty($this->agence)) $this->agence->removeBo($this);
    }

    /**
     * @return mixed
     */
    public function getCommunes()
    {
        return $this->communes;
    }

    /**
     * @param mixed $communes
     */
    public function setCommunes($communes)
    {
        $this->communes = $communes;
    }

    /**
     * @param Commune $commune
     */
    public function addCommune(Commune $commune)
    {
        $this->communes->add($commune);
        $commune->addBo($this);
    }

    /**
     * @param Commune $commune
     */
    public function removeCommune(Commune $commune)
    {
        $this->communes->removeElement($commune);
        $commune->removeBo($this);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }
}
