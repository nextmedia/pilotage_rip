<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActionRepository")
 * @ORM\Table(name="new_action")
 */
class Action
{
    CONST TYPE_AGENCE = "agence";
    CONST TYPE_BO = "bo";
    CONST TYPE_CATEGORIE = "categorie";
    CONST TYPE_CHANTIER = "chantier";
    CONST TYPE_COMMENTAIRE = "commentaire";
    CONST TYPE_COMMUNE = "commune";
    CONST TYPE_DR = "dr";
    CONST TYPE_REVUE = "revue";
    CONST TYPE_TYPEWORK = "typework";
    CONST TYPE_USER = "user";

    const ACTION = [
        self::TYPE_AGENCE => [
            "create" => "__WHO__ a créé l'agence '__NAME__'.",
            "edit" => "__WHO__ a modifié l'agence '__NAME__'.",
            "remove" => "__WHO__ a supprimé l'agence '__NAME__'."
        ],
        self::TYPE_BO => [
            "create" => "__WHO__ a créé le BO '__NAME__'.",
            "edit" => "__WHO__ a modifié le BO '__NAME__'.",
            "remove" => "__WHO__ a supprimé le BO '__NAME__'."
        ],
        self::TYPE_CATEGORIE => [
            "create" => "__WHO__ a créé la catégorie '__NAME__'.",
            "edit" => "__WHO__ a modifié la catégorie '__NAME__'.",
            "remove" => "__WHO__ a supprimé la catégorie '__NAME__'."
        ],
        self::TYPE_CHANTIER => [
            "close" => "__WHO__ a cloturé le chantier '__NAME__'.",
            "create" => "__WHO__ a créé le chantier '__NAME__'.",
            "edit" => "__WHO__ a modifié le chantier '__NAME__'.",
            "open" => "__WHO__ a ouvert le chantier '__NAME__'.",
            "remove" => "__WHO__ a supprimé le chantier '__NAME__'."
        ],
        self::TYPE_COMMENTAIRE => [
            "create" => "__WHO__ a créé un commentaire sur le chantier '__NAME__'.",
        ],
        self::TYPE_COMMUNE => [
            "create" => "__WHO__ a créé la commune '__NAME__'.",
            "edit" => "__WHO__ a modifié la commune '__NAME__'.",
            "remove" => "__WHO__ a supprimé la commune '__NAME__'."
        ],
        self::TYPE_DR => [
            "create" => "__WHO__ a créé la DR '__NAME__'.",
            "edit" => "__WHO__ a modifié la DR '__NAME__'.",
            "remove" => "__WHO__ a supprimé la DR '__NAME__'."
        ],
        self::TYPE_REVUE => [
            "close" => "__WHO__ a cloturé la revue de '__NAME__'."
        ],
        self::TYPE_TYPEWORK => [
            "create" => "__WHO__ a créé le type de travail '__NAME__'.",
            "edit" => "__WHO__ a modifié le type de travail '__NAME__'.",
            "remove" => "__WHO__ a supprimé le type de travail '__NAME__'."
        ],
        self::TYPE_USER => [
            "create" => "__WHO__ a créé l'utilisateur '__NAME__'.",
            "edit" => "__WHO__ a modifié l'utilisateur '__NAME__'.",
            "remove" => "__WHO__ a supprimé l'utilisateur '__NAME__'."
        ],
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $idSource;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $libelle;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="actions")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=true)
     */
    private $createdBy;

    /**
     * Action constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getIdSource()
    {
        return $this->idSource;
    }

    /**
     * @param mixed $idSource
     */
    public function setIdSource($idSource)
    {
        $this->idSource = $idSource;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }
}
