<?php

namespace App\Form;

use App\Entity\Agence;
use App\Entity\BO;
use App\Entity\DR;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FilterType extends AbstractType
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * CommuneType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dr', EntityType::class, [
                "label" => $this->translator->trans("dr.libelle"),
                "class" => DR::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('agence', EntityType::class, [
                "label" => $this->translator->trans("agency.libelle", ["%count%" => 1]),
                "class" => Agence::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('bo', EntityType::class, [
                "label" => $this->translator->trans("bo.libelle"),
                "class" => BO::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => $this->translator->trans("utils.search"),
                "attr" => [
                    "class" => "btn btn-success btn-sm"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
