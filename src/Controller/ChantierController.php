<?php

namespace App\Controller;

use App\Entity\Activite;
use App\Entity\Agence;
use App\Entity\Categorie;
use App\Entity\Chantier;
use App\Entity\Commune;
use App\Entity\User;
use App\Form\ChantierType;
use App\Form\UploadFileType;
use App\Repository\ChantierRepository;
use App\Repository\UserRepository;
use App\Service\ChantierService;
use App\Service\UtilsService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ChantierController
 * @package App\Controller
 * @Route("/chantier", name="site_")
 */
class ChantierController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param ChantierRepository $chantierRepository
     * @return Response
     */
    public function main(ChantierRepository $chantierRepository)
    {
        $chantiers = $chantierRepository->findAll();

        return $this->render("chantier/main.html.twig", [
            "chantiers" => $chantiers
        ]);
    }

    /**
     * @Route("/mes-chantiers", name="mine")
     * @param ChantierRepository $chantierRepository
     * @return Response
     */
    public function mySites(ChantierRepository $chantierRepository)
    {
        /** @var User $user */
        $user = $this->getUser();
        $chantiers = $chantierRepository->findByUser($user);

        return $this->render("chantier/main.html.twig", [
            "chantiers" => $chantiers
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param ChantierService $chantierService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        ChantierService $chantierService,
        TranslatorInterface $translator
    ) {
        $chantier = new Chantier();

        $chantierForm = $this->createForm(ChantierType::class, $chantier);
        $chantierForm->handleRequest($request);

        if ($chantierForm->isSubmitted() && $chantierForm->isValid()) {
            $chantierService->init($chantier);
            $this->addFlash("success", $translator->trans("site.sentence.new_success"));
            return $this->redirectToRoute("site_info", ["id" => $chantier->getId()]);
        }

        return $this->render("chantier/info.html.twig", [
            "chantierForm" => $chantierForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param Request $request
     * @param Chantier $chantier
     * @param ChantierService $chantierService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function info(
        Request $request,
        Chantier $chantier,
        ChantierService $chantierService,
        TranslatorInterface $translator
    ) {
        /** @var User $user */
        $user = $this->getUser();
        if(!empty($user) && $user->hasRole(User::ROLE_RIP)) {
            if ($chantier->getPreparateur() !== $user) {
                $this->addFlash("danger", $translator->trans("error.access_denied"));
                return $this->redirectToRoute("site_mine");
            }
        }

        $chantierForm = $this->createForm(ChantierType::class, $chantier);
        $chantierForm->handleRequest($request);

        if ($chantierForm->isSubmitted() && $chantierForm->isValid()) {
            $chantierService->edit($chantier);

            $this->addFlash("success", $translator->trans("utils.edit_success"));

            return $this->redirectToRoute("site_info", ["id" => $chantier->getId()]);
        }

        return $this->render("chantier/info.html.twig", [
            "chantier" => $chantier,
            "chantierForm" => $chantierForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/details", name="details", options={"expose"=true})
     * @param Chantier $chantier
     * @return Response
     */
    public function details(Chantier $chantier)
    {
        $chantierForm = $this->createForm(ChantierType::class, $chantier);

        return $this->render("chantier/_details.html.twig", [
            "chantier" => $chantier,
            "chantierForm" => $chantierForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param Chantier $chantier
     * @param ChantierService $chantierService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        Chantier $chantier,
        ChantierService $chantierService,
        TranslatorInterface $translator
    ) {
        $chantierService->delete($chantier);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("site_main");
    }

    /**
     * @Route("/{id}/close", name="close")
     * @param Chantier $chantier
     * @param ChantierService $chantierService
     * @return RedirectResponse
     */
    public function close(Chantier $chantier, ChantierService $chantierService)
    {
        $chantierService->close($chantier);

        return $this->redirectToRoute("site_info", ["id" => $chantier->getId()]);
    }

    /**
     * @Route("/{id}/closeAjax", name="close_ajax", options={"expose"=true})
     * @param Chantier $chantier
     * @param ChantierService $chantierService
     * @param UtilsService $utilsService
     * @return JsonResponse
     */
    public function closeAjax(
        Chantier $chantier,
        ChantierService $chantierService,
        UtilsService $utilsService
    ) {
        $chantierService->close($chantier);

        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);

        return new JsonResponse($response);
    }

    /**
     * @Route("/{id}/openAjax", name="open_ajax", options={"expose"=true})
     * @param Chantier $chantier
     * @param ChantierService $chantierService
     * @param UtilsService $utilsService
     * @return JsonResponse
     */
    public function openAjax(
        Chantier $chantier,
        ChantierService $chantierService,
        UtilsService $utilsService
    ) {
        $chantierService->open($chantier);

        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);

        return new JsonResponse($response);
    }

    /**
     * @Route("/{id}/addCommentary", name="add_commentary", options={"expose"=true})
     * @param Chantier $chantier
     * @param Request $request
     * @param ChantierService $chantierService
     * @param UtilsService $utilsService
     * @return JsonResponse
     */
    public function addCommentary(
        Chantier $chantier,
        Request $request,
        ChantierService $chantierService,
        UtilsService $utilsService
    ) {
        $message = $request->get("message");

        $commentary = $chantierService->addCommentary($chantier, $message);
        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);

        $response["date"] = $commentary->getCreatedAt()->format("d/m/Y");
        $response["user"] = $commentary->getUser()->getFirstname() . " " . $commentary->getUser()->getLastname();
        $response["message"] = $commentary->getMessage();

        return new JsonResponse($response);
    }

    /**
     * @Route("/{id}/edit", name="edit", options={"expose"=true})
     * @param Request $request
     * @param Chantier $chantier
     * @param ChantierService $chantierService
     * @param UtilsService $utilsService
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @return JsonResponse
     */
    public function edit(
        Request $request,
        Chantier $chantier,
        ChantierService $chantierService,
        UtilsService $utilsService,
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ) {
        /** @var User $user */
        $user = $this->getUser();
        if(!empty($user) && $user->hasRole(User::ROLE_RIP)) {
            if ($chantier->getPreparateur() !== $user) {
                $response = $utilsService->getJsonResponse(false, $translator->trans("error.access_denied"));
                return new JsonResponse($response);
            }
        }

        $infosChantier = [];
        foreach($request->get("values") as $value) {
            $infosChantier[$value["name"]] = $value["value"];
        }
        $chantierService->edit($chantier, $infosChantier);

        /** @var Commune $commune */
        $commune = $em->getRepository(Commune::class)->find($infosChantier["chantier[commune]"]);
        $infosChantier["chantier[commune]"] = strval($commune);

        /** @var Categorie $categorie */
        $categorie = $em->getRepository(Categorie::class)->find($infosChantier["chantier[categorie]"]);
        $infosChantier["chantier[categorie]"] = strval($categorie);

        $response = $utilsService->getJsonResponse(true, $translator->trans("utils.edit_success"));
        $response["infosChantier"] = $infosChantier;

        return new JsonResponse($response);
    }

    /**
     * @Route("/import", name="import")
     * @param Request $request
     * @param ChantierService $chantierService
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function import(
        Request $request,
        ChantierService $chantierService,
        EntityManagerInterface $em
    ) {
        $agences = $em->getRepository(Agence::class)->findAll();

        $form = $this->createForm(UploadFileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();
            $agenceId = $form->getExtraData()["agence"];

            try {
                foreach ($datas["fichier"] as $fichier) {
                    $chantierService->import($fichier, $agenceId);
                }
            } catch (Exception $e) {
                $this->addFlash("danger", $e->getMessage());
                return $this->redirectToRoute("site_import");
            }
            return $this->redirectToRoute("site_main");
        }

        return $this->render("chantier/import.html.twig", [
            "form" => $form->createView(),
            "agences" => $agences
        ]);
    }

    /**
     * @Route("/attribution", name="assignement")
     * @param ChantierRepository $chantierRepository
     * @return Response
     */
    public function assignement(
        ChantierRepository $chantierRepository
    ) {
        $chantier = $chantierRepository->findUnassigned();

        return $this->render("chantier/assignement.html.twig", [
            "chantiers" => $chantier
        ]);
    }

    /**
     * @Route("/getChargesUsers", name="get_charges_users", options={"expose"=true})
     * @param ChantierService $chantierService
     * @param UserRepository $userRepository
     * @param Request $request
     * @return Response
     */
    public function getChargesUsers(
        ChantierService $chantierService,
        UserRepository $userRepository,
        Request $request,
        ChantierRepository $chantierRepository
    ) {
        $users = $userRepository->findByRole(User::ROLE_RIP);

        $start = DateTime::createFromFormat('d/m/Y', $request->get("start"));
        $end = DateTime::createFromFormat('d/m/Y', $request->get("end"));

        foreach($users as $user) {
            $startClone = clone $start;
            $endClone = clone $end;

            $open = count($chantierRepository->findByUserAndDate($user, $start, $end));
            $charge = $chantierService->calculCharge($user, $startClone, $endClone);

            $response[$user->getId()]["identity"] = $user->getIdentity();
            $response[$user->getId()]["dispo"] = $charge["dispo"];
            $response[$user->getId()]["charge"] = $charge["charge"];
            $response[$user->getId()]["open"] = $open;
        }

        return $this->render("user/_charges.html.twig", [
            "users" => $users,
            "datas" => $response
        ]);
    }

    /**
     * @Route("/{id}/load_graphs", name="load_graphs", options={"expose"=true})
     * @param User $user
     * @param UtilsService $utilsService
     * @param ChantierService $chantierService
     * @param ChantierRepository $chantierRepository
     * @return JsonResponse
     */
    public function loadGraphs(
        User $user,
        UtilsService $utilsService,
        ChantierService $chantierService,
        ChantierRepository $chantierRepository
    ) {
        $start = new DateTime();
        $end = new DateTime();
        $end->modify("+1 month");
        $open = count($chantierRepository->findByUserAndDate($user, $start, $end));
        $closed = count($chantierRepository->findByUserAndDateFinished($user, $start, $end));

        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);
        $response["dispo"] = $chantierService->getTimeDispoMonthInYear($user);
        $response["prepa"] = $chantierService->getTimePrepaMonthInYear($user);
        $response["open"] = $open;
        $response["closed"] = $closed;

        return new JsonResponse($response);
    }
}