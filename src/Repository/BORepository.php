<?php

namespace App\Repository;

use App\Entity\BO;
use App\Entity\Agence;
use App\Entity\DR;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method BO|null find($id, $lockMode = null, $lockVersion = null)
 * @method BO|null findOneBy(array $criteria, array $orderBy = null)
 * @method BO[]    findAll()
 * @method BO[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BORepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BO::class);
    }

    /**
     * @param Agence $agence
     * @return QueryBuilder
     */
    public function findAvailablesBo(Agence $agence)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("b")
            ->from(BO::class, "b")
            ->leftJoin("b.agence", "a")
            ->where("a.id = :id")
            ->orWhere("a.id IS NULL")
            ->setParameter("id", $agence->getId())
        ;

        return $query;
    }

    /**
     * @param DR $dr
     * @return int|mixed|string
     */
    public function findByDR(DR $dr)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('bo')
            ->from(BO::class, 'bo')
            ->leftJoin("bo.agence", "agence")
            ->leftJoin("agence.dr", "dr")
            ->where("dr.id = :id")
            ->setParameter("id", $dr->getId())
        ;

        return $qb->getQuery()->getResult();
    }
}
