<?php

namespace App\Controller;

use LogicException;
use App\Entity\Commune;
use App\Form\CommuneType;
use App\Service\CommuneService;
use App\Repository\CommuneRepository;
use App\Repository\ChantierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CommuneController
 * @package App\Controller
 * @Route("/commune", name="municipality_")
 */
class CommuneController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param CommuneRepository $communeRepository
     * @return Response
     */
    public function main(CommuneRepository $communeRepository)
    {
        $communes = $communeRepository->findAll();

        return $this->render("commune/main.html.twig", [
            "communes" => $communes
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param CommuneService $communeService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        CommuneService $communeService,
        TranslatorInterface $translator
    ) {
        $commune = new Commune();

        $communeForm = $this->createForm(CommuneType::class, $commune);
        $communeForm->handleRequest($request);

        if ($communeForm->isSubmitted() && $communeForm->isValid()) {
            try {
                $communeService->init($commune);
                $this->addFlash("success", $translator->trans("municipality.sentence.new_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
                return $this->redirectToRoute("municipality_new");
            }

            return $this->redirectToRoute("municipality_info", ["id" => $commune->getId()]);
        }

        return $this->render("commune/info.html.twig", [
            "communeForm" => $communeForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param Request $request
     * @param Commune $commune
     * @param CommuneService $communeService
     * @param ChantierRepository $chantierRepository
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function info(
        Request $request,
        Commune $commune,
        CommuneService $communeService,
        ChantierRepository $chantierRepository,
        TranslatorInterface $translator
    ) {
        $communeForm = $this->createForm(CommuneType::class, $commune);
        $communeForm->handleRequest($request);

        if ($communeForm->isSubmitted() && $communeForm->isValid()) {
            try {
                $communeService->edit($commune);
                $this->addFlash("success", $translator->trans("utils.edit_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirectToRoute("municipality_info", ["id" => $commune->getId()]);
        }

        $chantiers = $chantierRepository->findBy(["commune" => $commune->getId()]);

        return $this->render("commune/info.html.twig", [
            "commune" => $commune,
            "chantiers" => $chantiers,
            "communeForm" => $communeForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param Commune $commune
     * @param CommuneService $communeService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        Commune $commune,
        CommuneService $communeService,
        TranslatorInterface $translator
    ) {
        $communeService->delete($commune);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("municipality_main");
    }
}