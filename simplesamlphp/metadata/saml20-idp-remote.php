<?php
$metadata['idp://PROD-STANDARD.MULTIAUTH-SESAME-ENEDIS/20211106'] = array (
  'entityid' => 'idp://PROD-STANDARD.MULTIAUTH-SESAME-ENEDIS/20211106',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/SSORedirect/metaAlias/multiauth/idp5',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/SSOPOST/metaAlias/multiauth/idp5',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/SSOSoap/metaAlias/multiauth/idp5',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/IDPSloPOST/metaAlias/multiauth/idp5',
      'ResponseLocation' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/IDPSloPOST/metaAlias/multiauth/idp5',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/IDPSloRedirect/metaAlias/multiauth/idp5',
      'ResponseLocation' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/IDPSloRedirect/metaAlias/multiauth/idp5',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/IDPSloSoap/metaAlias/multiauth/idp5',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://websso-gardian.myelectricnetwork.com:443/gardianwebsso/ArtifactResolver/metaAlias/multiauth/idp5',
      'index' => 0,
      'isDefault' => true,
    ),
  ),
  'NameIDFormats' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
    1 => 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
    2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIFfTCCA2WgAwIBAgICL7cwDQYJKoZIhvcNAQELBQAwZjELMAkGA1UEBhMCRlIxHjAcBgNVBAoM
FUVMRUNUUklDSVRFIERFIEZSQU5DRTEXMBUGA1UECwwOMDAwMiA1NTIwODEzMTcxHjAcBgNVBAMM
FUFDIElORlJBU1RSVUNUVVJFIEVERjAeFw0xOTA0MTgxMTQ2MDlaFw0yMTExMDYwOTU4MjFaMGMx
CzAJBgNVBAYTAkZSMR4wHAYDVQQKDBVFTEVDVFJJQ0lURSBERSBGUkFOQ0UxFzAVBgNVBAsMDjAw
MDIgNTUyMDgxMzE3MRswGQYDVQQDDBJpZHAtZ2FyZGlhbi5lZGYuZnIwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDqvAAE6nZ5ZHvD+LQ+MBq31kjT1mM5AeePNVEPmyi0zYLuHDNOYwPu
8l8nEh9Tt7IJNdQk0H4oowgbsCqwH/8FXH3hrJwQ0ADRQIDtPL4AlnwtzwomLPAUS44HhnveaJrn
Igg5CrweoiqMA5pOPgzb8NOa2nDtiVlm1Xlamz32S8NFbq0ZLoMT8L1xud1TyUBZCbea3YKeR5AN
AiCuf82XDzHQuR/NROHXtb+Wc0Kfr5dqMC+NqgyPhcBcAtwcZBrn2gnD/4/WzS7hqxUvbWPrBH3K
7aGhjAFUqR8Z02ViwoOljs1qfAJKJUBq9wGyMChSZAkAxwa64pO7c02pO1bDAgMBAAGjggE2MIIB
MjAdBgNVHQ4EFgQUYBf09SAGLlQ6SdVetDUI58r0JeswHwYDVR0jBBgwFoAU2r/OzvQ08pw/Wb/+
dOMShlEZU4UwXAYDVR0gBFUwUzBRBg4qgXoBgXAAAQECARYBATA/MD0GCCsGAQUFBwIBFjFodHRw
Oi8vcGMtZWRmLmVkZi5mci9wYy9hY19pbmZyYXN0cnVjdHVyZV9lZGYucGRmMB0GA1UdJQQWMBQG
CCsGAQUFBwMCBggrBgEFBQcDATAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0RBBYwFIISaWRwLWdhcmRp
YW4uZWRmLmZyMEQGA1UdHwQ9MDswOaA3oDWGM2h0dHA6Ly9jcmwtZWRmLmVkZi5mci9jcmwvYWNf
aW5mcmFzdHJ1Y3R1cmVfZWRmLmNybDANBgkqhkiG9w0BAQsFAAOCAgEAlP/WB58boJKGNo3LWFfC
Z/A8Ga+riebcPnP42m1bLlAz0esaEYfSG3PsitgFwP3SAYXaI2YDPXNPHMTwJWEAbUHvbAx3eJyO
zZq79fdo0l67g2icbMStifhEAaegcIeP6iAmdlkClRdKofkZI0dy2Ri/FGqUH9G4wKO2qL9cfiJh
7mQ5Hc9ps9hAxBOSgBOMr2ItOeuQQACkiKHjfYpaJBDZw1BjQXEdSeBj9Lxl5te4hYJk5isyaH+x
f9g8AmyzyQMnZ0zVQHWN+AalEgouPJNvh+9MA/7e9M7pjgZB+YHKDvIQaMiip9UhCqDVRF1RXUSG
ZKaRJEyjSzm9I85mwnusCqb8VOmYr6+W7Go1fHg+8cHm1yRYeZZDsGVuvHak7ZHRL8wdQt4kYOsI
vQYZTrLgr+Es/99rDaVOnaA8DN5iTnR6Uv++TS5UTdnh+PL66uTFLFUehp5x8FLN/ZnrBTH+VJ4O
uV1u/D2gAHt+VmsfB8iz996Rv/AwreHx+xXVFa3OOLpYueaXx5qyI4aY8DEko3QValREaLePaneR
3/5l1vpsznUMtb8S7UfiBG/xJZLlCW84S6hDx2A9zHRI5O1P+A4lP0xOp837rm22dbxsMDYEFMW/
AJSmX+/MrxaqK0eW0WKzReoCYJq47jcHeQscF26efoC+iS2cxfjq15g=
                    ',
    ),
  ),
);
?>