<?php

namespace App\Repository;

use App\Entity\TypeTravail;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method TypeTravail|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeTravail|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeTravail[]    findAll()
 * @method TypeTravail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeTravailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeTravail::class);
    }
}
