<?php

namespace App\Service;

use App\Entity\Revue;
use App\Entity\User;
use App\Repository\RevueRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use PhpOffice\PhpSpreadsheet;

class RevueService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var KernelInterface $kernel */
    private $kernel;


    /**
     * AgenceService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param KernelInterface $kernel
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        KernelInterface $kernel
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->kernel = $kernel;
    }

    /**
     * Envoi de l'email de revu
     *
     * @param Revue $revue
     * @param String $filePath
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function sendRevue(Revue $revue, String $filePath)
    {
        /** @var User $createdBy */
        $createdBy = $revue->getCreatedBy();
        /** @var User $createdFor */
        $createdFor = $revue->getCreatedFor();

        if( !empty($createdBy->getEmail()) && filter_var($createdBy->getEmail(), FILTER_VALIDATE_EMAIL)){
            $mail = new PHPMailer();
            $mail->setFrom("suivi-rip@enedis-grdf.fr", "Suivi RIP");
            $mail->addAddress($createdBy->getEmail());
            if (!empty($createdFor->getEmail()))
                $mail->addCC($createdFor->getEmail());
            $mail->isHTML(true);
            $mail->Subject = "Revue de chantier";
            $mail->Body = empty($revue->getCommentaire()->getMessage()) ? "Rapport de revue" : $revue->getCommentaire()->getMessage();

            if( file_exists($filePath) ){
                $mail->addAttachment($filePath, "rapport_revue.xlsx");
            }

            $mail->send();
        }
    }

    /**
     * Génération du rapport de revue
     *
     * @param $chantiers
     * @return false|string
     * @throws PhpSpreadsheet\Exception
     */
    public function createRapportRevue($chantiers){
        $phpExcelObject = new PhpSpreadsheet\Spreadsheet();

        //On génère le header
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', "Numéro" )
            ->setCellValue('B1', "Code Affaire" )
            ->setCellValue('C1', "Intitulé" )
            ->setCellValue('D1', "Etat" )
            ->setCellValue('E1', "Préparateur" )
            ->setCellValue('F1', "Priorité" )
            ->setCellValue('G1', "Type de chantier" )
            ->setCellValue('H1', "Commune" )
            ->setCellValue('I1', "Adresse" )
            ->setCellValue('J1', "Catégorie" )
            ->setCellValue('K1', "Préparation théorique (en h)" )
            ->setCellValue('L1', "Préparation retenue (en h)" )
            ->setCellValue('M1', "Préparation réalisée (en h)" )
            ->setCellValue('N1', "Temps restant" )
            ->setCellValue('O1', "Date création" )
            ->setCellValue('P1', "Début préparation" )
            ->setCellValue('Q1', "Fin préparation" )
            ->setCellValue('R1', "Début réalisation" )
            ->setCellValue('S1', "Fin réalisation" )
            ->setCellValue('T1', "Responsable" )
            ->setCellValue('U1', "Nom du demandeur" )
            ->setCellValue('V1', "Entité Demandeur" )
            ->setCellValue('W1', "Entité Préparateur" )
            ->setCellValue('X1', "Entité Réalisatrice" )
            ->setCellValue('Y1', "Nom du CA" )
            ->setCellValue('Z1', "Commentaires" );

        $activeSheet = $phpExcelObject->getActiveSheet();
        $activeSheet->setTitle('Rapport de revue');

        //On affiche une ligne par chantier
        $ligne = 2;
        foreach ($chantiers as $chantier){
            $activeSheet->setCellValue( ('A' . (String)$ligne), $chantier->getNumber() );
            $activeSheet->setCellValue( ('B' . (String)$ligne), $chantier->getCodeAffaire() );
            $activeSheet->setCellValue( ('C' . (String)$ligne), $chantier->getIntitule());
            $activeSheet->setCellValue( ('D' . (String)$ligne), $chantier->getEtat() );
            $activeSheet->setCellValue( ('E' . (String)$ligne), $chantier->getPreparateur()?$chantier->getPreparateur()->getFullName():null );
            $activeSheet->setCellValue( ('F' . (String)$ligne), $chantier->getPriorite() );
            $activeSheet->setCellValue( ('G' . (String)$ligne), $chantier->getType() );
            $activeSheet->setCellValue( ('H' . (String)$ligne), $chantier->getCommune()?$chantier->getCommune()->getLibelleComplet():null );
            $activeSheet->setCellValue( ('I' . (String)$ligne), $chantier->getRue() );
            $activeSheet->setCellValue( ('J' . (String)$ligne), $chantier->getCategorie()?$chantier->getCategorie()->getLibelle():null );
            $activeSheet->setCellValue( ('K' . (String)$ligne), $chantier->getPrepaTheory() );
            $activeSheet->setCellValue( ('L' . (String)$ligne), $chantier->getPrepaRetenue() );
            $activeSheet->setCellValue( ('M' . (String)$ligne), $chantier->getPrepaRealisee() );
            $activeSheet->setCellValue( ('N' . (String)$ligne), $chantier->getTempsRestant() );
            $activeSheet->setCellValue( ('O' . (String)$ligne), $chantier->getCreatedAt()?$chantier->getCreatedAt()->format("d/m/Y"):null);
            $activeSheet->setCellValue( ('P' . (String)$ligne), $chantier->getDateDebutPrepa()?$chantier->getDateDebutPrepa()->format("d/m/Y"):null);
            $activeSheet->setCellValue( ('Q' . (String)$ligne), $chantier->getDateFinPrepa()?$chantier->getDateFinPrepa()->format("d/m/Y"):null);
            $activeSheet->setCellValue( ('R' . (String)$ligne), $chantier->getDateDebutReal()?$chantier->getDateDebutReal()->format("d/m/Y"):null);
            $activeSheet->setCellValue( ('S' . (String)$ligne), $chantier->getDateFinReal()?$chantier->getDateFinReal()->format("d/m/Y"):null);
            $activeSheet->setCellValue( ('T' . (String)$ligne), $chantier->getResponsable()?$chantier->getResponsable()->getFullName():null );
            $activeSheet->setCellValue( ('U' . (String)$ligne), $chantier->getNomDemandeur() );
            $activeSheet->setCellValue( ('V' . (String)$ligne), $chantier->getEntiteDemandeur() );
            $activeSheet->setCellValue( ('W' . (String)$ligne), $chantier->getEntitePrepa()?$chantier->getEntitePrepa()->getName():null );
            $activeSheet->setCellValue( ('X' . (String)$ligne), $chantier->getEntiteReal()?$chantier->getEntiteReal()->getName():null );
            $activeSheet->setCellValue( ('Y' . (String)$ligne), $chantier->getNomCA() );
            $activeSheet->setCellValue( ('Z' . (String)$ligne), $this->generateListeCommentaires($chantier->getCommentaires()) );

            $ligne++;
        }

        //Création de la réponse
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($phpExcelObject, "Xlsx");

        $fileName = 'rapport_revue.xlsx';
        $path = $this->kernel->getProjectDir() . "/uploads/tmp/" . $fileName;

        $writer->save($path);

        return $path;
    }

    /**
     * On génére une ligne formatée pour les commentaires du chantier
     *
     * @param $commentaires
     * @return string
     */
    private function generateListeCommentaires( $commentaires ){
        $retour = "";
        foreach ($commentaires as $commentaire){
            $retour .= $commentaire->getUser() . ' - ' . $commentaire->getCreatedAt()->format("d/m/Y") . ' - ' . $commentaire->getMessage() . ';';
        }
        return $retour;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getLastyReviewByUser(User $user)
    {
        /** @var RevueRepository $reviewRepository */
        $reviewRepository = $this->em->getRepository(Revue::class);
        /** @var Revue $lastReview */
        return $reviewRepository->findLastReviewUser($user);
    }
}