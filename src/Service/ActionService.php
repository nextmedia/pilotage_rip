<?php

namespace App\Service;

use App\Entity\Action;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActionService
{
    /**@var EntityManagerInterface $em*/
    private $em;

    /** @var TranslatorInterface $translator */
    private $translator;

    /**
     * AdresseService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * @param $type
     * @param $who
     * @param $what
     * @param $object
     * @param null $options
     */
    public function newAction($type, $who, $what, $object, $options = null)
    {
        $action = new Action();
        $action->setCreatedAt(new DateTime());

        $action->setCreatedBy($who);
        $action->setType($type);
        $action->setIdSource($object->getId());

        $action->setLibelle($this->replaceConstantes($who, $what, $object, $options));

        $this->em->persist($action);
        $this->em->flush();
    }

    /**
     * @param $who
     * @param $what
     * @param $object
     * @param $options
     * @return string|string[]
     */
    private function replaceConstantes($who, $what, $object, $options)
    {
        $libelle = $what;
        $libelle = str_replace("__WHO__", $who->getIdentity(), $libelle);
        $libelle = str_replace("__NAME__", $object->__toString(), $libelle);

        if (!empty($options["toWho"])) {
            $user = $options["toWho"];
            $libelle = str_replace("__TO_WHO__", $user->getIdentity(), $libelle);
        }

        return $libelle;
    }
}