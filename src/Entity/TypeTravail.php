<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeTravailRepository")
 * @ORM\Table(name="new_typetravail")
 */
class TypeTravail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $libelle;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $duration;

    /**
     * @ORM\OneToMany(targetEntity="Chantier", mappedBy="typeTravail")
     */
    private $chantiers;

    /**
     * TypeChantier constructor.
     */
    public function __construct()
    {
        $this->chantiers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getChantiers()
    {
        return $this->chantiers;
    }

    /**
     * @param mixed $chantiers
     */
    public function setChantiers($chantiers)
    {
        $this->chantiers = $chantiers;
    }

    /**
     * @param Chantier $chantier
     */
    public function addChantier(Chantier $chantier)
    {
        $this->chantiers->add($chantier);
    }

    /**
     * @param Chantier $chantier
     */
    public function removeChantier(Chantier $chantier)
    {
        $this->chantiers->removeElement($chantier);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->libelle;
    }
}
