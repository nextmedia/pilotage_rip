<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DRRepository")
 * @ORM\Table(name="new_dr")
 */
class DR
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Agence", mappedBy="dr")
     */
    private $agences;

    /**
     * DR constructor.
     */
    public function __construct()
    {
        $this->agences = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAgences()
    {
        return $this->agences;
    }

    /**
     * @param ArrayCollection $agences
     */
    public function setAgences(ArrayCollection $agences)
    {
        $this->agences = $agences;
    }

    /**
     * @param Agence $agence
     */
    public function addAgence(Agence $agence)
    {
        $this->agences->add($agence);
        $agence->setDr($this);
    }

    /**
     * @param Agence $agence
     */
    public function removeAgence(Agence $agence)
    {
        $this->agences->removeElement($agence);
        $agence->setDr(null);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }
}
