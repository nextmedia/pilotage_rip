<?php

namespace App\Repository;

use App\Entity\BO;
use App\Entity\Commune;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Commune|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commune|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commune[]    findAll()
 * @method Commune[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommuneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commune::class);
    }

    /**
     * @param BO $bo
     * @return QueryBuilder
     */
    public function findAvailablesCommunes(BO $bo)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("c")
            ->from(Commune::class, "c")
            ->leftJoin("c.listBo", "b")
            ->where("b.id = :id")
            ->orWhere("b.id IS NULL")
            ->setParameter("id", $bo->getId())
        ;

        return $query;
    }

    /**
     * @param BO $bo
     * @return QueryBuilder
     */
    public function findByBo(BO $bo)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("c")
            ->from(Commune::class, "c")
            ->leftJoin("c.listBo", "b")
            ->where("b.id = :id")
            ->setParameter("id", $bo->getId())
        ;

        return $query;
    }
}
