<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\BO;
use App\Entity\Commune;
use App\Repository\CommuneRepository;
use LogicException;
use App\Repository\BORepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class BoService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var BORepository $boRepository */
    private $boRepository;
    /** @var Security $security */
    private $security;
    /** @var ActionService $actionService */
    private $actionService;

    /**
     * BoService constructor.
     * @param EntityManagerInterface $em
     * @param BORepository $boRepository
     * @param TranslatorInterface $translator
     * @param Security $security
     * @param ActionService $actionService
     */
    public function __construct(
        EntityManagerInterface $em,
        BORepository $boRepository,
        TranslatorInterface $translator,
        Security $security,
        ActionService $actionService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->boRepository = $boRepository;
        $this->security = $security;
        $this->actionService = $actionService;
    }

    /**
     * @param BO $bo
     */
    public function init(BO $bo)
    {
        $searchBo = $this->boRepository->findOneBy(["name" => $bo->getName(), "agence" => $bo->getAgence()]);
        if (!empty($searchBo)) {
            throw new LogicException($this->translator->trans("error.bo.name_exist"));
        }

        if (!empty($bo->getCommunes())) {
            /** @var Commune $commune */
            foreach($bo->getCommunes()->getValues() as $commune) {
                $bo->addCommune($commune);
            }
        }

        $this->em->persist($bo);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_BO,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_BO]["create"],
            $bo
        );
    }

    /**
     * @param BO $bo
     */
    public function edit(BO $bo)
    {
        $searchBo = $this->boRepository->findOneBy(["name" => $bo->getName(), "agence" => $bo->getAgence()]);
        if (!empty($searchBo) && $bo->getId() != $searchBo->getId()) {
            throw new LogicException($this->translator->trans("error.bo.name_exist"));
        }

        /** @var CommuneRepository $communeRepository */
        $communeRepository = $this->em->getRepository(Commune::class);

        $communes = $communeRepository->findByBo($bo)->getQuery()->getResult();

        /** @var Commune $commune */
        foreach($communes as $commune) {
            if (!in_array($commune, $bo->getCommunes()->getValues())) {
                $commune->removeBo($bo);
            }
        }

        /** @var Commune $commune */
        foreach($bo->getCommunes() as $commune) {
            $bo->addCommune($commune);
        }

        $this->em->persist($bo);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_BO,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_BO]["edit"],
            $bo
        );
    }

    /**
     * @param BO $bo
     */
    public function delete(BO $bo)
    {
        $this->actionService->newAction(
            Action::TYPE_BO,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_BO]["remove"],
            $bo
        );

        $this->em->remove($bo);
        $this->em->flush();
    }
}