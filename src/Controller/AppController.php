<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UtilsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class AppController
 * @package App\Controller
 * @Route("/", name="app_")
 */
class AppController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function homepage()
    {
        /** @var User $user */
        $user = $this->getUser();

        if (empty($user)) { return $this->redirectToRoute("app_login"); }

        return $user->hasRole(User::ROLE_RIP) ? $this->redirectToRoute("site_mine") : $this->redirectToRoute("site_main");
    }

    /**
     * @Route("/getMessageFlash", name="message_flash", options={"expose"=true})
     * @param Request $request
     * @return Response
     */
    public function getMessageFlash(Request $request)
    {
        $type = $request->get("type");
        $message = $request->get("message");

        $this->addFlash($type, $message);

        return $this->render("utils/_messageFlash.html.twig");
    }

    /**
     * @Route("/filter-review", name="filter_review", options={"expose"=true})
     * @param Request $request
     * @param UtilsService $utilsService
     * @return JsonResponse
     */
    public function filterReview(Request $request, UtilsService $utilsService)
    {
        $id = $request->get("id");
        $type = $request->get("type");
        $dr = $request->get("dr");

        $response = $utilsService->getDataFilter($id, $type, $dr);

        return new JsonResponse($response);
    }

    /**
     * @Route("/login", name="login")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param EventDispatcherInterface $eventDispatcher
     * @param TokenStorageInterface $tokenStorage
     * @return Response
     */
    public function login(
        UserRepository $userRepository,
        Request $request,
        EventDispatcherInterface $eventDispatcher,
        TokenStorageInterface $tokenStorage
    ) {
//        session_start();
//        $session = \SimpleSAML\Session::getSessionFromRequest();
//        $session->cleanup();
//
//        $params = ['ReturnTo' => 'https://suivi-rip.appheb.enedis-grdf.fr:19419'];
//        $as = new Simple("default-sp");
//
//        try {
//            $as->requireAuth($params);
//            $attributes = $as->getAttributes();
//            $nni = $attributes['gardianwebsso_id'][0];
//
//            /** @var User $user */
//            $user = $userRepository->findOneBy(["nni" => $nni]);
//
//            if (!empty($user)) {
//                $token = new UsernamePasswordToken($user, null, "public", $user->getRoles());
//                $tokenStorage->setToken($token);
//                $event = new InteractiveLoginEvent($request, $token);
//                $eventDispatcher->dispatch($event);
//
//                return $this->redirectToRoute("app_homepage");
//            } else {
//                return $this->redirectToRoute("app_login");
//            }
//        } catch (Exception $e) {
//            $this->addFlash("warning", $e->getMessage());
//        }

        /** @var User $user */
        $user = $userRepository->findOneBy(["nni" => "A19171"]);

        $token = new UsernamePasswordToken($user, null, "public", $user->getRoles());
        $tokenStorage->setToken($token);
        $event = new InteractiveLoginEvent($request, $token);
        $eventDispatcher->dispatch($event);

        return $this->redirectToRoute("app_homepage");
    }

    /**
     * Peut être laissé vide
     * @Route("/logout", name="logout")
     */
    public function logout() {}

    /**
     * @Route("/getLogbook", name="logbook", options={"expose"=true})
     * @param Request $request
     * @param UtilsService $utilsService
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function getLogbook(
        Request $request,
        UtilsService $utilsService,
        EntityManagerInterface $em
    ) {
        $id = $request->get("idSource");
        $objectClass= $request->get("objectClass");
        $type = $request->get("type");

        $object = $em->getRepository("App\Entity\\".$objectClass)->find($id);
        $actions = $utilsService->getActionsByObjectAndDate($object, $type);

        return $this->render("utils/_logbook.html.twig", [
            "actions" => $actions
        ]);
    }
}