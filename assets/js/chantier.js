const routes = require('./routing/fos_js_routes.json'),
    bootbox = require( './bootbox.all.min.js');

import Chart from '../../node_modules/chart.js/dist/chart';
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import {changeOption} from "./filtre";
import {getMessageFlash, initDatatables} from "./main";
Routing.setRoutingData(routes);

export {addCommentary, selectTypeTravail, changeTime, closeSite, onValidate, openSite, showCharge, selectPrepaCharge, clearDate};

$(function() {
    addCommentary();
    selectTypeTravail();
    changeTime();
    closeSite();
    onValidate();
    managePrepa();
    openSite();
    showCharge();
    chart();
    clearDate();
});

/**
 * Change les heures théoriques à la selection d'un type de travail
 */
function selectTypeTravail() {
    var $typeTravail = $("#chantier_typeTravail"),
        $inputTheory = $("#prepaTheory");

    $typeTravail.on("change", function() {
       var that = $(this),
           url = Routing.generate("typework_duration", {"id" : that.val()});

       if (that.val().length > 0) {
           $.ajax({
               url : url,
               type : "POST",
               dataType : "json",
               success : function(data){
                   var duration = data.duration;
                   $inputTheory.html(duration);
               }
           });
       }
    });
}

/**
 * Ajoute un commentaire sur un chantier
 */
function addCommentary() {
    var $btn = $(".btn-commentaire"),
        $textarea = $("#Commentaire"),
        $table = $("#TableCommentaire");

    $btn.off("click").on("click", function() {
        var that = $(this),
            url = Routing.generate("site_add_commentary", {"id" : that.data("id"), "message" : $textarea.val()});

        $.ajax({
            url : url,
            type : "POST",
            dataType : "json",
            success : function(data){
                var row = "<tr>" +
                    "<td class='text-center'>"+data.date+"</td>" +
                    "<td>"+data.user+"</td>" +
                    "<td>"+data.message+"</td>" +
                    "</tr>";
                $table.find("tbody").append(row);
                $textarea.val("");
            }
        });
    });
}

/**
 * Gère les calculs aux changements de durée
 */
function changeTime() {
    var $tempsRestant = $("#TempsRestant"),
        $tempsRetenu = $("#chantier_prepaRetenue"),
        $tempsReal = $("#chantier_prepaRealisee");

    if ($tempsRetenu.length > 0 && $tempsReal.length > 0) {
        calculTime($tempsRestant, $tempsRetenu, $tempsReal);
    }

    $tempsRetenu.off("change").on("change", function() {
        calculTime($tempsRestant, $tempsRetenu, $tempsReal);
    });
    $tempsReal.off("change").on("change", function() {
        calculTime($tempsRestant, $tempsRetenu, $tempsReal);
    });
}

/**
 * Calcul le temps restant
 */
function calculTime($tempsRestant, $tempsRetenu, $tempsReal) {
    var val = 0;

    if ($tempsRetenu.val() > 0){
        if ($tempsReal.val() > 0) {
            val = $tempsRetenu.val() - $tempsReal.val();
        } else {
            val = $tempsRetenu.val();
        }
    }

    $tempsRestant.html(val);
}

/**
 * Cloture un chantier
 */
function closeSite() {
    var $btn = $(".btn-close");

    $btn.off("click").on("click", function() {
        var that = $(this),
            id = that.data("id"),
            url = Routing.generate("site_close_ajax", {"id" : id});

        bootbox.confirm({
            animate: false,
            message: "Voulez-vous cloturer ce chantier ?",
            backdrop: true,
            buttons: {
                confirm: {
                    label: "Cloturer",
                    className: "btn-sm btn-warning"
                },
                cancel: {
                    label: "Annuler",
                    className: "btn-sm btn-primary"
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url : url,
                        type : "POST",
                        dataType : "json",
                        success : function(data){
                            if(that.data("page") === "info"){
                                var message = getMessageFlash("success", "Chantier cloturé.");

                                that.closest(".box-header").prepend(message);

                                if ($("#FirstBox").length > 0) {
                                    var $row = $("tr[data-id='"+id+"']");

                                    $row.find(".site-state").removeClass(function (index, className) {
                                        return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
                                    });
                                    $row.find(".site-state").addClass("bg-orange")
                                    $row.find(".btn-close").remove();
                                }
                            } else {
                                // Enleve les classes bg-...
                                that.closest("tr").find(".site-state").removeClass(function (index, className) {
                                    return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
                                });
                                that.closest("tr").find(".site-state").addClass("bg-orange");
                            }
                            that.remove();
                        }
                    });
                }
            }
        });
    });
}

/**
 * A la validation des changements d'un chantier
 */
function onValidate() {
    var $btn = $("#ValidateChantier");

    $btn.off("click").on("click", function() {
        var that = $(this),
            values = $("form").serializeArray(),
            id = that.data("id"),
            url = Routing.generate("site_edit", {"id" : id, "values" : values});

        $.ajax({
            url : url,
            type : "POST",
            dataType : "json",
            success : function(data){
                var message = "";
                if (data.status === true) {
                    var row = $("tr[data-id="+id+"]"),
                        infos = data.infosChantier;

                    message = getMessageFlash("success", data.message);

                    row.find("td[data-info=number]").html(infos["chantier[number]"]);
                    row.find("td[data-info=codeAffaire]").html(infos["chantier[codeAffaire]"]);
                    row.find("td[data-info=intitule]").html(infos["chantier[intitule]"]);
                    row.find("td[data-info=etat]").html(infos["chantier[etat]"]);
                    row.find("td[data-info=commune]").html(infos["chantier[commune]"]);
                    row.find("td[data-info=categorie]").html(infos["chantier[categorie]"]);
                    row.find("td[data-info=dateFinReal]").html(infos["chantier[dateFinReal]"]);
                } else {
                    message = getMessageFlash("danger", data.message);
                }
                that.closest(".box-header").prepend(message);
            }
        });
    });
}

/**
 * Gère la liste des RIP concernés
 */
function managePrepa() {
    var $selectBo = $("#chantier_entitePrepa"),
        $btn = $("#SeeAllUsers"),
        $preparateurs = $("#chantier_preparateur");

    $selectBo.on("change", function() {
        var that = $(this),
            url = Routing.generate("user_get_users", {"idBo" : that.val()}),
            userSelected = $preparateurs.val();

        $.ajax({
            url : url,
            type : "POST",
            dataType : "json",
            success : function(data){
                changeOption(data.users, $preparateurs);
                $preparateurs.val(userSelected);
            }
        });
    });

    $btn.off("click").on("click", function() {
        var url = Routing.generate("user_get_users", {}),
            userSelected = $preparateurs.val();

        $.ajax({
            url : url,
            type : "POST",
            dataType : "json",
            success : function(data){
                changeOption(data.users, $preparateurs);
                $preparateurs.val(userSelected);
            }
        });
    });
}

/**
 * Réouvre un chantier
 */
function openSite() {
    var $btn = $(".btn-open");

    $btn.off("click").on("click", function() {
        var that = $(this),
            id = that.data("id"),
            url = Routing.generate("site_open_ajax", {"id": id});

        bootbox.confirm({
            animate: false,
            message: "Voulez-vous ouvrir ce chantier ?",
            backdrop: true,
            buttons: {
                confirm: {
                    label: "Ouvrir",
                    className: "btn-sm btn-warning"
                },
                cancel: {
                    label: "Annuler",
                    className: "btn-sm btn-primary"
                }
            },
            callback: function (result) {
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    success: function () {
                        $("#DivClose").remove();
                        that.remove();
                    }
                });
            }
        });

    });
}

/**
 * Affiche les charges des RIP
 */
function showCharge() {
    var $btn = $(".btn-charge");

    $btn.off("click").on("click", function() {
        var date = new Date(),
            dateStart = new Date(),
            dateEnd = new Date(date.setMonth(date.getMonth()+1)),
            start = String(dateStart.getDate()).padStart(2, '0') + "/" + String(dateStart.getMonth() + 1).padStart(2, '0') + "/" + dateStart.getFullYear(),
            end = String(dateEnd.getDate()).padStart(2, '0') + "/" + String(dateEnd.getMonth() + 1).padStart(2, '0') + "/" + dateEnd.getFullYear(),
            dialog = bootbox.dialog({message: '<p><i class="fa fa-spin fa-spinner"></i></p>', size: "large"});

        dialog.init(function() {
            var url = Routing.generate("site_get_charges_users", {"start": start, "end": end}),
                $body = dialog.find('.bootbox-body');

            $.ajax({
                url : url,
                type : "POST",
                dataType : "html",
                success : function(data){
                    $body.html("");
                    $body.append(data);
                    initDatatables();
                    changeDateCharges();
                    selectPrepaCharge();
                }
            });
        });
    });
}

/**
 * Change le créneau du calcul de charge
 */
function changeDateCharges() {
    var $btn = $(".select-time"),
        $body = $(".bootbox-body");

    $btn.off("click").on("click", function() {
        var that = $(this),
            start = that.data("start"),
            end = that.data("end"),
            duration = that.data("duration");

        $body.html('<p><i class="fa fa-spin fa-spinner"></i></p>');

        $.ajax({
            url : Routing.generate("site_get_charges_users", {"start": start, "end": end}),
            type : "POST",
            dataType : "html",
            success : function(data){
                $body.html("");
                $body.html(data);
                initDatatables();
                changeDateCharges();
                $body.find(".select-time").each(function() {
                    if ($(this).hasClass("btn-primary")) {
                        $(this).removeClass("btn-primary");
                        $(this).addClass("btn-default");
                    }
                });
                $("a[data-duration='"+duration+"']").removeClass("btn-default").addClass("btn-primary");
            }
        });
    });
}

/**
 * Charge le graph
 */
function chart() {
    var $chartBar = $("#ChartBar"),
        $chartPie = $('#ChartPie')

    if ($chartBar.length > 0 || $chartPie.length > 0) {
        var userId = $("input[data-user]").data("user"),
            data = {
                datasets: [
                    {
                        label: "Temps disponible",
                        backgroundColor: "#3c8dbc"
                    },
                    {
                        label: "Temps de prépa",
                        backgroundColor: "#55c618"
                    }
                ]
            },
            monthNow = new Date().getMonth(),
            months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        data["labels"] = months.splice(monthNow, 12).concat(months.slice(0, monthNow));

        $.ajax({
            url: Routing.generate("site_load_graphs", {"id": userId}),
            type: "POST",
            dataType: "json",
            success: function (result) {
                var dispo = [],
                    prepa = [],
                    yearNow = new Date().getFullYear();

                // Attribue les valeurs qui vont être envoyés dans le graph
                for (var index = 1; index < 13; index++) {
                    var monthArray = (index + monthNow) % 12

                    // Comme il y a un modulo 12, Décembre est à 0 sans cette ligne
                    if (monthArray === 0) monthArray = 12

                    // Nécessite le 0 devant pour avoir accès au tableau
                    if (monthArray < 10) { monthArray = "0" + monthArray }

                    // Défini l'année à utiliser dans l'attribution des valeurs
                    var year = monthArray >= monthNow + 1 ? yearNow : yearNow + 1

                    // Attribue les valeurs en fonction des mois et de l'existence des données
                    year in result.dispo && monthArray in result.dispo[year] ? dispo.push(result.dispo[year][monthArray]) : dispo.push(0)
                    year in result.prepa && monthArray in result.prepa[year] ? prepa.push(result.prepa[year][monthArray]) : prepa.push(0)
                }

                data["datasets"][0]["data"] = dispo;
                data["datasets"][1]["data"] = prepa;

                const configBar = {
                    type: 'bar',
                    data: data,
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    },
                };

                // GraphPie
                var PieData = {
                        labels: [
                            'Dossiers en cours',
                            'Dossiers cloturés'
                        ],
                        datasets: [{
                            data: [result.open, result.closed],
                            backgroundColor: ["#55c618", "#db2b2b"],
                            hoverBackgroundColor: ["#63e71c", "#fa3030"]
                        }],
                    };
                var configPie = {
                        type: 'doughnut',
                        data: PieData
                    };

                window.chartBar = new Chart(document.getElementById('ChartBar'), configBar);
                window.chartPie = new Chart(document.getElementById('ChartPie'), configPie);
            }
        });
    }
}

/**
 * A la selection d'un préparateur dans l'affichage des charges
 */
function selectPrepaCharge() {
    var $btn = $(".select-prepa"),
        $prepa = $("#chantier_preparateur");

    $btn.off("click").on("click", function() {
       var that = $(this),
           id = that.parent().data("id");

        $prepa.val(id);
        $prepa.trigger("change");
        $(".bootbox-close-button").click();
    });
}

/**
 * Mets à vide une date
 */
function clearDate() {
    var $trash = $("[data-date]")

    $trash.off("click").on("click", function() {
        var number = $(this).data("date"),
            $cible = $("[data-cible='"+number+"']")

        $cible.find("input").val("")
    })
}