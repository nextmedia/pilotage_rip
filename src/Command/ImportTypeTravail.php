<?php


namespace App\Command;

use App\Entity\TypeTravail;
use App\Repository\TypeTravailRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportTypeTravail extends Command
{
    protected static $defaultName = 'import:travail';

    /** @var EntityManagerInterface $em */
    private $em;
    /** @var KernelInterface $kernel */
    private $kernel;

    /**
     * ImportTypeTravail constructor.
     * @param EntityManagerInterface $em
     * @param KernelInterface $kernel
     */
    public function __construct(
        EntityManagerInterface $em,
        KernelInterface $kernel
    ) {
        $this->em = $em;
        $this->kernel = $kernel;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Import des types de travaux')

            ->addArgument('name', InputArgument::REQUIRED, 'nom du fichier')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Import des types de travaux.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Import en cours ...");

        try {
            $this->import($input);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return -1;
        }

        $output->writeln("Terminé !");

        return 0;
    }

    /**
     * @param InputInterface $input
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function import(InputInterface $input)
    {
        $path = $this->kernel->getProjectDir() . "/uploads";

        $name = $input->getArgument("name");
        $fileName = "/".$name;

        if (!file_exists($path.$fileName)){
            throw new \PhpOffice\PhpSpreadsheet\Exception($path.$fileName);
        }

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);

        $spreadsheet = $reader->load($path.$fileName);

        $worksheet = $spreadsheet->getActiveSheet();

        foreach ($worksheet->getRowIterator() as $row) {
            if($row->getRowIndex() === 1) continue;

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);

            $arrayLine = [];
            foreach ($cellIterator as $cell) {
                $arrayLine[$cell->getColumn()] = $cell->getValue();
            }

            $typeTravail = new TypeTravail();
            $typeTravail->setLibelle($arrayLine["A"]);
            $typeTravail->setDuration($arrayLine["B"]);

            $this->em->persist($typeTravail);
        }
        $this->em->flush();
    }
}