<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="new_user")
 */
class User implements UserInterface
{
    CONST ROLE_RIP = "ROLE_RIP";
    CONST ROLE_PILOTE = "ROLE_PILOTE";
    CONST ROLE_CHEF_AGENCE = "ROLE_CHEF_AGENCE";
    CONST ROLE_ADMIN = "ROLE_ADMIN";
    CONST ROLES = [
        "RIP" => self::ROLE_RIP,
        "Pilote" => self::ROLE_PILOTE,
        "Chef d'agence" => self::ROLE_CHEF_AGENCE,
        "Administrateur" => self::ROLE_ADMIN
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nni;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $lastname;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateLastReview;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=2, nullable=false)
     */
    private $coefficient;

    /**
     * @ORM\OneToMany(targetEntity="CommentaireChantier", mappedBy="user")
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity="Planning", mappedBy="user")
     */
    private $plannings;

    /**
     * @ORM\OneToMany(targetEntity="Chantier", mappedBy="responsable")
     */
    private $chantiersResponsable;

    /**
     * @ORM\OneToMany(targetEntity="Chantier", mappedBy="preparateur")
     */
    private $chantiersPreparateur;

    /**
     * @ORM\OneToMany(targetEntity="Revue", mappedBy="createdBy")
     */
    private $revues;

    /**
     * @ORM\OneToMany(targetEntity="Revue", mappedBy="createdFor")
     */
    private $revuesCreated;

    /**
     * @ORM\ManyToOne(targetEntity="BO", inversedBy="users")
     */
    private $bo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Action", mappedBy="createdBy", cascade={"persist"})
     */
    private $actions;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->coefficient = 1;
        $this->commentaires = new ArrayCollection();
        $this->chantiersResponsable = new ArrayCollection();
        $this->chantiersPreparateur = new ArrayCollection();
        $this->revues = new ArrayCollection();
        $this->actions = new ArrayCollection();
        $this->plannings = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @see UserInterface
     */
    public function getUsername()
    {
        return $this->firstname . " " . $this->lastname;
    }

    /**
     * @see UserInterface
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @param string $role
     * @return User
     */
    public function addRole(string $role)
    {
        $role = strtoupper($role);

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return null;
    }

    /** @see UserInterface */
    public function eraseCredentials() {}

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getDateLastReview()
    {
        return $this->dateLastReview;
    }

    /**
     * @param mixed $dateLastReview
     */
    public function setDateLastReview($dateLastReview)
    {
        $this->dateLastReview = $dateLastReview;
    }

    /**
     * @return float
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * @param $coefficient
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * @param CommentaireChantier $commentaire
     */
    public function addCommentaire(CommentaireChantier $commentaire)
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires->add($commentaire);
        }
    }

    /**
     * @param CommentaireChantier $commentaire
     */
    public function removeCommentaire(CommentaireChantier $commentaire)
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
        }
    }

    /**
     * @return mixed
     */
    public function getChantiersResponsable()
    {
        return $this->chantiersResponsable;
    }

    /**
     * @param mixed $chantiersResponsable
     */
    public function setChantiersResponsable($chantiersResponsable)
    {
        $this->chantiersResponsable = $chantiersResponsable;
    }

    /**
     * @param Chantier $chantier
     */
    public function addChantierResponsable(Chantier $chantier)
    {
        if (!$this->chantiersResponsable->contains($chantier)) {
            $this->chantiersResponsable->add($chantier);
        }
    }

    /**
     * @param Chantier $chantier
     */
    public function removeChantierResponsable(Chantier $chantier)
    {
        if ($this->chantiersResponsable->contains($chantier)) {
            $this->chantiersResponsable->removeElement($chantier);
        }
    }

    /**
     * @return mixed
     */
    public function getChantiersPreparateur()
    {
        return $this->chantiersPreparateur;
    }

    /**
     * @param mixed $chantiersPreparateur
     */
    public function setChantiersPreparateur($chantiersPreparateur)
    {
        $this->chantiersPreparateur = $chantiersPreparateur;
    }

    /**
     * @param Chantier $chantier
     */
    public function addChantierPreparateur(Chantier $chantier)
    {
        if (!$this->chantiersPreparateur->contains($chantier)) {
            $this->chantiersPreparateur->add($chantier);
        }
    }

    /**
     * @param Chantier $chantier
     */
    public function removeChantierPreparateur(Chantier $chantier)
    {
        if ($this->chantiersPreparateur->contains($chantier)) {
            $this->chantiersPreparateur->removeElement($chantier);
        }
    }

    /**
     * @return string
     */
    public function getIdentity()
    {
        return $this->getFirstname() . " " . $this->getLastname();
    }

    /**
     * @return mixed
     */
    public function getBo()
    {
        return $this->bo;
    }

    /**
     * @param mixed $bo
     */
    public function setBo($bo)
    {
        $this->bo = $bo;
    }

    /**
     * @return ArrayCollection
     */
    public function getRevues()
    {
        return $this->revues;
    }

    /**
     * @param ArrayCollection $revues
     */
    public function setRevues(ArrayCollection $revues)
    {
        $this->revues = $revues;
    }

    /**
     * @return mixed
     */
    public function getRevuesCreated()
    {
        return $this->revuesCreated;
    }

    /**
     * @param mixed $revuesCreated
     */
    public function setRevuesCreated($revuesCreated)
    {
        $this->revuesCreated = $revuesCreated;
    }


    /**
     * @return mixed
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param mixed $actions
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
    }

    /**
     * @param Action $action
     */
    public function addAction(Action $action)
    {
        $this->actions->add($action);
    }

    /**
     * @param Action $action
     */
    public function removeAction(Action $action)
    {
        $this->actions->removeElement($action);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->firstname . " " . $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNni()
    {
        return $this->nni;
    }

    /**
     * @param mixed $nni
     */
    public function setNni($nni)
    {
        $this->nni = $nni;
    }

	/**
	 * Retourne le nom complet
	 *
	 * @return string
	 */
    public function getFullName(){
    	return $this->getLastname() . ' ' . $this->getFirstname();
	}

    /**
     * @param string $role
     * @return bool
     */
    public function hasRole(string $role)
    {
        return in_array($role, $this->roles);
    }

    /**
     * @return ArrayCollection
     */
    public function getPlannings()
    {
        return $this->plannings;
    }

    /**
     * @param ArrayCollection $plannings
     */
    public function setPlannings(ArrayCollection $plannings)
    {
        $this->plannings = $plannings;
    }

    /**
     * @param Planning $planning
     */
    public function addPlanning(Planning $planning)
    {
        $this->plannings->add($planning);
    }

    /**
     * @param Planning $planning
     */
    public function removePlanning(Planning $planning)
    {
        $this->plannings->removeElement($planning);
    }
}
