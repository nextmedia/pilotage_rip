<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Agence;
use App\Entity\BO;
use App\Entity\Categorie;
use App\Entity\Chantier;
use App\Entity\CommentaireChantier;
use App\Entity\Commune;
use App\Entity\Planning;
use App\Entity\TypeTravail;
use App\Entity\User;
use App\Repository\CategorieRepository;
use App\Repository\ChantierRepository;
use App\Repository\CommentaireChantierRepository;
use App\Repository\CommuneRepository;
use App\Repository\PlanningRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ChantierService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var ChantierRepository $chantierRepository */
    private $chantierRepository;
    /** @var Security $security */
    private $security;
    /** @var ActionService $actionService */
    private $actionService;
    /** @var UserService $userService */
    private $userService;

    /**
     * CommuneService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param ChantierRepository $chantierRepository
     * @param Security $security
     * @param ActionService $actionService
     * @param UserService $userService
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        ChantierRepository $chantierRepository,
        Security $security,
        ActionService $actionService,
        UserService $userService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->chantierRepository = $chantierRepository;
        $this->security = $security;
        $this->actionService = $actionService;
        $this->userService = $userService;
    }

    /**
     * @param Chantier $chantier
     */
    public function init(Chantier $chantier)
    {
        $this->em->persist($chantier);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_CHANTIER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CHANTIER]["create"],
            $chantier
        );
    }

    /**
     * @param Chantier $chantier
     * @param array|null $infos
     */
    public function edit(Chantier $chantier, array $infos = null)
    {
        if (!empty($infos)) {
            $boRepository = $this->em->getRepository(BO::class);
            $categorieRepository = $this->em->getRepository(Categorie::class);
            $communeRepository = $this->em->getRepository(Commune::class);
            $userRepository = $this->em->getRepository(User::class);
            $typeTravailRepository = $this->em->getRepository(TypeTravail::class);

            $chantier->setNumber($infos["chantier[number]"]);
            $chantier->setCodeAffaire($infos["chantier[codeAffaire]"]);
            $chantier->setIntitule($infos["chantier[intitule]"]);
            $chantier->setEtat($infos["chantier[etat]"]);
            $chantier->setPreparateur($userRepository->find($infos["chantier[preparateur]"]));
            if (!empty($infos["chantier[priorite]"])){
                $chantier->setPriorite($infos["chantier[priorite]"]);
            } else {
                $chantier->setPriorite(0);
            }
            $chantier->setType($infos["chantier[type]"]);
            $chantier->setCommune($communeRepository->find($infos["chantier[commune]"]));
            $chantier->setRue($infos["chantier[rue]"]);
            $chantier->setComplement($infos["chantier[complement]"]);
            $chantier->setCategorie($categorieRepository->find($infos["chantier[categorie]"]));
            if (!empty($infos["chantier[prepaRetenue]"])) {
                $chantier->setPrepaRetenue($infos["chantier[prepaRetenue]"]);
            } else {
                $chantier->setPrepaRetenue(0);
            }
            if (!empty($infos["chantier[prepaRealisee]"])) {
                $chantier->setPrepaRealisee($infos["chantier[prepaRealisee]"]);
            } else {
                $chantier->setPrepaRealisee(0);
            }
            if (!empty($infos["chantier[dateDebutPrepa]"])) {
                $chantier->setDateDebutPrepa(date_create_from_format("d/m/Y", $infos["chantier[dateDebutPrepa]"]));
            } else {
                $chantier->setDateDebutPrepa(null);
            }
            if (!empty($infos["chantier[dateFinPrepa]"])){
                $chantier->setDateFinPrepa(date_create_from_format("d/m/Y", $infos["chantier[dateFinPrepa]"]));
            } else {
                $chantier->setDateFinPrepa(null);
            }
            if (!empty($infos["chantier[dateDebutReal]"])) {
                $chantier->setDateDebutReal(date_create_from_format("d/m/Y", $infos["chantier[dateDebutReal]"]));
            } else {
                $chantier->setDateDebutReal(null);
            }
            if (!empty($infos["chantier[dateFinReal]"])) {
                $chantier->setDateFinReal(date_create_from_format("d/m/Y", $infos["chantier[dateFinReal]"]));
            } else {
                $chantier->setDateFinReal(null);
            }
            $chantier->setResponsable($userRepository->find($infos["chantier[responsable]"]));
            $chantier->setNomDemandeur($infos["chantier[nomDemandeur]"]);
            $chantier->setEntiteDemandeur($infos["chantier[entiteDemandeur]"]);
            $chantier->setEntitePrepa($boRepository->find($infos["chantier[entitePrepa]"]));
            $chantier->setEntiteReal($boRepository->find($infos["chantier[entiteReal]"]));
            $chantier->setNomCA($infos["chantier[nomCA]"]);
            $chantier->setTypeTravail($typeTravailRepository->find($infos["chantier[typeTravail]"]));

            if (!empty($chantier->getTypeTravail())) {
                $chantier->setPrepaTheory($chantier->getTypeTravail()->getDuration());
            } else {
                $chantier->setPrepaTheory(0);
            }
        }

        $this->em->persist($chantier);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_CHANTIER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CHANTIER]["edit"],
            $chantier
        );
    }

    /**
     * @param Chantier $chantier
     */
    public function delete(Chantier $chantier)
    {
        $this->actionService->newAction(
            Action::TYPE_CHANTIER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CHANTIER]["remove"],
            $chantier
        );

        $this->em->remove($chantier);
        $this->em->flush();
    }

    /**
     * @param Chantier $chantier
     */
    public function close(Chantier $chantier)
    {
        $chantier->setDateCloture(new DateTime());
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_CHANTIER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CHANTIER]["close"],
            $chantier
        );
    }

    /**
     * @param Chantier $chantier
     * @param string $message
     * @return CommentaireChantier
     */
    public function addCommentary(Chantier $chantier, string $message)
    {
        $commentaire = new CommentaireChantier();
        $commentaire->setMessage($message);
        $commentaire->setChantier($chantier);
        $commentaire->setUser($this->security->getUser());

        $this->em->persist($commentaire);
        $chantier->addCommentaire($commentaire);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_COMMENTAIRE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_COMMENTAIRE]["create"],
            $chantier
        );

        return $commentaire;
    }

    /**
     * @param BO $bo
     * @return int|mixed|string
     */
    public function getChantierByBo(BO $bo)
    {
        /** @var ChantierRepository $chantierRepository */
        $chantierRepository = $this->em->getRepository(Chantier::class);

        return $chantierRepository->findByBo($bo);
    }

    /**
     * @param Chantier $chantier
     * @return int|mixed|string
     */
    public function getLastCommentary(Chantier $chantier)
    {
        /** @var CommentaireChantierRepository $commentaryRepository */
        $commentaryRepository = $this->em->getRepository(CommentaireChantier::class);
        /** @var CommentaireChantier $commentaire */
        $commentaire = $commentaryRepository->findLast($chantier)[0];

        return $commentaire->getMessage();
    }

    /**
     * @param Chantier $chantier
     */
    public function open(Chantier $chantier)
    {
        $chantier->setDateCloture(null);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_CHANTIER,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_CHANTIER]["open"],
            $chantier
        );
    }

    /**
     * @param UploadedFile $fichier
     * @param $agenceId
     * @throws Exception|\PhpOffice\PhpSpreadsheet\Exception
     */
    public function import(UploadedFile $fichier, $agenceId)
    {
        /** @var CommuneRepository $communeRepository */
        $communeRepository = $this->em->getRepository(Commune::class);
        /** @var ChantierRepository $chantierRepository */
        $chantierRepository = $this->em->getRepository(Chantier::class);
        /** @var CategorieRepository $categorieRepository */
        $categorieRepository = $this->em->getRepository(Categorie::class);

        if (!empty($agenceId)) {
            /** @var Agence $agence */
            $agence = $this->em->getRepository(Agence::class)->find($agenceId);
        }

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);

        $spreadsheet = $reader->load($fichier->getRealPath());
        $worksheet = $spreadsheet->getActiveSheet();

        // Méthodes qui vont passer dans une boucle
        $methodesEach = [
            "Code affaire" => "setCodeAffaire",
            "Type de chantier" => "setType",
            "Etat" => "setEtat",
            "Demandeur" => "setNomDemandeur",
            "Entité demandeur" => "setEntiteDemandeur",
            "Commentaire" => "setCommentaireCinkeP",
            "Intitulé" => "setIntitule",
            "Nom du CA" => "setNomCA",
        ];

        // Méthodes qui ont besoin de plus de traitement qui ne peuvent pas être dans une boucle
        $methodes = [
            "Catégorie" => "setCategorie",
            "Commune" => "setCommune",
            "Nom Préparateur 1" => "setPreparateur",
            "Entité Préparateur 1" => "setEntitePrepa",
            "Entité de réalisation" => "setEntiteReal",
            "Responsable du chantier" => "setResponsable",
            "Début de chantier" => "setDateDebutPrepa",
            "Fin de chantier" => "setDateFinReal",
            "Fin de préparation" => "setDateFinPrepa",
            "Date de création Affaire" => "setCreatedAt",
            "Urgence" => "setPriorite",
            "Rue" => "setRue"
        ];
        $references = [];

        foreach ($worksheet->getRowIterator() as $row) {
            $flush = false;
            $arrayLine = [];
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);

            if ($row->getRowIndex() === 1) {
                foreach ($cellIterator as $cell) {
                    $references[$cell->getValue()] = $cell->getColumn();
                }
                continue;
            }
            foreach ($cellIterator as $cell) $arrayLine[$cell->getColumn()] = $cell->getValue();

            if (array_key_exists("Numéro", $references) && empty($arrayLine[$references["Numéro"]])
                || array_key_exists("Etat", $references) && $arrayLine[$references["Etat"]] === "Annulé") continue;

            $chantier = $chantierRepository->findOneBy(["number" => $arrayLine[$references["Numéro"]]]);
            $newSite = empty($chantier);
            if (empty($chantier)) {
                $chantier = new Chantier();
                $chantier->setNumber($arrayLine[$references["Numéro"]]);
            }

            foreach ($methodesEach as $key => $method) {
                if (array_key_exists($key, $references) && !empty($arrayLine[$references[$key]]))
                    $this->callMethod($chantier, $method, $arrayLine[$references[$key]]);
            }

            if (array_key_exists("Catégorie", $references) && !empty($arrayLine[$references["Catégorie"]])) {
                $categorie = $categorieRepository->findOneBy(["libelle" => $arrayLine[$references["Catégorie"]]]);
                if (empty($categorie)) {
                    $categorie = new Categorie();
                    $categorie->setLibelle($arrayLine[$references["Catégorie"]]);
                    $this->em->persist($categorie);
                    $flush = true;
                }
                $this->callMethod($chantier, $methodes["Catégorie"], $categorie);
            }
            if (array_key_exists("Numéro de l'adresse", $references) && !empty($arrayLine[$references["Numéro de l'adresse"]])
                || array_key_exists("Rue", $references) && !empty($arrayLine[$references["Rue"]])) {
                $libelleAdresse = array_key_exists("Numéro de l'adresse", $references) ? $arrayLine[$references["Numéro de l'adresse"]] . " " : "";
                $libelleAdresse .= array_key_exists("Rue", $references) ? $arrayLine[$references["Rue"]] : "";

                $this->callMethod($chantier, $methodes["Rue"], $libelleAdresse);
            }
            if (array_key_exists("Nom Préparateur 1", $references) && !empty(trim($arrayLine[$references["Nom Préparateur 1"]]))) {
                $user = $this->findOrCreateUser($arrayLine[$references["Nom Préparateur 1"]]);
                $chantier->setPreparateur($user);
            }
            if (array_key_exists("Entité Préparateur 1", $references) && !empty(trim($arrayLine[$references["Entité Préparateur 1"]]))) {
                $bo = $this->findBo($arrayLine[$references["Entité Préparateur 1"]]);
                $chantier->setEntitePrepa($bo);
                if (!empty($agence) && empty($bo->getAgence())) {
                    $bo->setAgence($agence);
                }
            }
            if (array_key_exists("Entité de réalisation", $references) && !empty(trim($arrayLine[$references["Entité de réalisation"]]))) {
                $bo = $this->findBo($arrayLine[$references["Entité de réalisation"]]);
                $chantier->setEntiteReal($bo);
                if (!empty($agence) && empty($bo->getAgence())) {
                    $bo->setAgence($agence);
                }
            }
            if (array_key_exists("Responsable du chantier", $references) && !empty(trim($arrayLine[$references["Responsable du chantier"]]))) {
                $user = $this->findOrCreateUser($arrayLine[$references["Responsable du chantier"]]);
                $this->callMethod($chantier, $methodes["Responsable du chantier"], $user);
            }
            if (array_key_exists("Commune", $references) && !empty($arrayLine[$references["Commune"]])
                || array_key_exists("Code INSEE", $references) && !empty($arrayLine[$references["Code INSEE"]])) {
                $commune = $communeRepository->findOneBy(["libelle" => $arrayLine[$references["Commune"]], "insee" => $arrayLine[$references["Code INSEE"]]]);
                if (empty($commune)) {
                    $commune = new Commune();
                    $commune->setLibelle($arrayLine[$references["Commune"]]);
                    $commune->setInsee($arrayLine[$references["Code INSEE"]]);
                    $this->em->persist($commune);
                    $flush = true;
                }
                $this->callMethod($chantier, $methodes["Commune"], $commune);
            }

            if (array_key_exists("Urgence", $references) && !empty($arrayLine[$references["Urgence"]]))
                $this->callMethod($chantier, $methodes["Urgence"], $arrayLine[$references["Urgence"]][1]);
            if (array_key_exists("Début de chantier", $references) && !empty($arrayLine[$references["Début de chantier"]]) && $newSite)
                $this->callMethod($chantier, $methodes["Début de chantier"], $this->getDateFromCell("Y-m-d", $arrayLine[$references["Début de chantier"]]));
            if (array_key_exists("Fin de chantier", $references) && !empty($arrayLine[$references["Fin de chantier"]]))
                $this->callMethod($chantier, $methodes["Fin de chantier"], $this->getDateFromCell("Y-m-d", $arrayLine[$references["Fin de chantier"]]));
            if (array_key_exists("Fin de préparation", $references) && !empty($arrayLine[$references["Fin de préparation"]]))
                $this->callMethod($chantier, $methodes["Fin de préparation"], $this->getDateFromCell("Y-m-d", $arrayLine[$references["Fin de préparation"]]));
            if (array_key_exists("Date de création Affaire", $references) && !empty($arrayLine[$references["Date de création Affaire"]]))
                $this->callMethod($chantier, $methodes["Date de création Affaire"], $this->getDateFromCell("d/m/Y", $arrayLine[$references["Date de création Affaire"]]));

            /** @var User $rip */
            $rip = $chantier->getPreparateur();
            if (!empty($rip) && empty($rip->getBo())) {
                if (!empty($chantier->getEntitePrepa())) {
                    $rip->setBo($chantier->getEntitePrepa());
                }
            }
            if (!empty($chantier->getTypeTravail())) {
                $chantier->setPrepaTheory($chantier->getTypeTravail()->getDuration());
            } else {
                $chantier->setPrepaTheory(0);
            }

            $this->em->persist($chantier);
            if ($flush) $this->em->flush();
        }
        $this->em->flush();
    }

    /**
     * @param $object
     * @param $methode
     * @param $param
     */
    private function callMethod($object, $methode, $param)
    {
        $object->$methode($param);
    }

    /**
     * @param $format
     * @param $cell
     * @return DateTime|false
     * @throws \Exception
     */
    private function getDateFromCell($format, $cell)
    {
        $date = date_create_from_format($format, trim($cell));
        if (empty($date)) {
            $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(trim($cell));
        }
        return $date;
    }

    /**
     * @param $identity
     * @return User
     */
    private function findOrCreateUser($identity)
    {
        $words = explode(' ', $identity);
        $firstName = str_replace("ë", "e", $words[1]);
        $lastname = str_replace("ë", "e", $words[0]);
        $user = $this->em->getRepository(User::class)->findOneByIdentity($firstName, $lastname);
        if (empty($user)) {
            $user = new User();
            $user->addRole(User::ROLE_RIP);
            $user->setFirstname($firstName);
            $user->setLastname($lastname);

            $this->userService->init($user);
        }
        return $user;
    }

    /**
     * @param $libelle
     * @return BO|object
     */
    private function findBO($libelle)
    {
        $words = explode(' ', $libelle);
        // Enlève le type de l'array
        array_shift($words);
        $name = "";
        foreach ($words as $word) {
            $name .= $word . " ";
        }
        $name = trim($name);

        $bo = $this->em->getRepository(BO::class)->findOneBy(["name" => $name]);
        if (empty($bo)) {
            $bo = new BO();
            $bo->setName($name);

            $this->em->persist($bo);
            $this->em->flush();
        }
        return $bo;
    }

    /**
     * Répartie les heures de chaques chantiers par semaine
     * Et retourne un tableau [année][numéro semaine] = temps à réalisé
     * @param $chantiers
     * @return array
     */
    public function getDistributionChantiersPerWeek($chantiers)
    {
        $result = [];
        /** @var Chantier $chantier */
        foreach($chantiers as $chantier) {
            $now = new DateTime();
            $start = $now > $chantier->getDateDebutPrepa() ? $now : $chantier->getDateDebutPrepa();
            $end = $chantier->getDateFinPrepa();
            $nbrWeek = ceil($start->diff($end)->days / 7);
            if ($nbrWeek == 0) $nbrWeek = 1;
            $tpsRealise = empty($chantier->getPrepaRealisee()) ? 0 : $chantier->getPrepaRealisee();
            $hourPerWeek = ($chantier->getPrepaRetenue() - $tpsRealise) / $nbrWeek;

            $start->modify("-1 week");
            for ($i=0; $i<$nbrWeek; $i++) {
                $start->modify("+1 week");
                if (empty($result[$start->format("Y")][$start->format("W")])) {
                    $result[$start->format("Y")][$start->format("W")] = $hourPerWeek;
                } else {
                    $result[$start->format("Y")][$start->format("W")] += $hourPerWeek;
                }
            }

        }
        return $result;
    }

    /**
     * @param User $user
     * @param $start
     * @param $end
     * @return array
     */
    public function calculCharge(User $user, $start, $end)
    {
        /** @var ChantierRepository $chantierRepository */
        $chantierRepository = $this->em->getRepository(Chantier::class);

        $dispo = $this->userService->getTimePrepa($user, $start, $end);
        $chantiers = $chantierRepository->findByUserAndDate($user, $start, $end);
        $repartition = $this->getDistributionChantiersPerWeek($chantiers);

        $charge = 0;
        $start->modify("-1 week");
        while ($start < $end) {
            $start->modify("+1 week");
            if ($start->format("W") === "53") {
                $year = $start->format("Y")-1;
            } else {
                $year = $start->format("Y");
            }
            if (!empty($repartition[$year][$start->format("W")]))
                $charge += $repartition[$year][$start->format("W")];
        }

        return [
          "dispo" => $dispo,
          "charge" => round($charge, 1) / $user->getCoefficient()
        ];
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function getSitesCloseLastReview(User $user)
    {
        /** @var ChantierRepository $chantierRepository */
        $chantierRepository = $this->em->getRepository(Chantier::class);

        return $chantierRepository->findSitesLastReview($user);
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function getTimePrepaMonthInYear(User $user)
    {
        $start = new DateTime();
        $end = new DateTime();
        $end = $end->modify("+1 year");

        $chantiers = $this->chantierRepository->findByUserAndDate($user, $start, $end);

        $result = [];
        /** @var Chantier $chantier */
        foreach ($chantiers as $chantier) {
            $start = $chantier->getDateDebutPrepa();
            $end = $chantier->getDateFinPrepa();

            $startCount = clone $start;
            $nbrMonth = 0;
            while ($startCount <= $end) {
                $nbrMonth ++;
                $startCount->modify('first day of next month');
            }

            $hourPerMonth = $nbrMonth != 0 ? ($chantier->getPrepaRetenue() - $chantier->getPrepaRealisee()) / $nbrMonth : $chantier->getPrepaRetenue() - $chantier->getPrepaRealisee();

            $start->modify("-1 month");
            for ($i=0; $i<$nbrMonth; $i++) {
                $start->modify("+1 month");
                if (empty($result[$start->format("Y")][$start->format("m")])) {
                    $result[$start->format("Y")][$start->format("m")] = round($hourPerMonth, 1);
                } else {
                    $result[$start->format("Y")][$start->format("m")] += round($hourPerMonth, 1);
                }
            }
        }

        return $result;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getTimeDispoMonthInYear(User $user)
    {
        /** @var PlanningRepository $planningRepository */
        $planningRepository = $this->em->getRepository(Planning::class);

        $start = new DateTime();
        $end = new DateTime();
        $end = $end->modify("+1 year");

        $plannings = $planningRepository->findByUserAndDate($user, $start, $end, true);

        $result = [];
        /** @var Planning $planning */
        foreach($plannings as $planning) {
            $start = $planning->getStart()->format("H") * 60 + $planning->getStart()->format("i");
            $end = $planning->getEnd()->format("H") * 60 + $planning->getEnd()->format("i");
            $hours = round(($end - $start) / 60, 1);

            if (empty($result[$planning->getStart()->format("Y")][$planning->getStart()->format("m")])) {
                $result[$planning->getStart()->format("Y")][$planning->getStart()->format("m")] = $hours;
            } else {
                $result[$planning->getStart()->format("Y")][$planning->getStart()->format("m")] += $hours;
            }
        }

        return $result;
    }
}