<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Chantier;
use App\Entity\TypeTravail;
use App\Repository\ChantierRepository;
use App\Repository\TypeTravailRepository;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class TypeTravailService
{
    /**@var EntityManagerInterface $em*/
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var ActionService $actionService */
    private $actionService;
    /** @var Security $security */
    private $security;
    /** @var TypeTravailRepository $typeTravailRepository */
    private $typeTravailRepository;

    /**
     * AdresseService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param ActionService $actionService
     * @param Security $security
     * @param TypeTravailRepository $typeTravailRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        ActionService $actionService,
        Security $security,
        TypeTravailRepository $typeTravailRepository
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->actionService = $actionService;
        $this->security = $security;
        $this->typeTravailRepository = $typeTravailRepository;
    }

    /**
     * @param TypeTravail $typeTravail
     */
    public function init(TypeTravail $typeTravail)
    {
        $searchTypeTravail = $this->typeTravailRepository->findOneBy(["libelle" => $typeTravail->getLibelle()]);
        if (!empty($searchTypeTravail)) {
            throw new LogicException($this->translator->trans("error.type_work.libelle_exist"));
        }

        $this->em->persist($typeTravail);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_TYPEWORK,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_TYPEWORK]["create"],
            $typeTravail
        );
    }

    /**
     * @param TypeTravail $typeTravail
     */
    public function edit(TypeTravail $typeTravail)
    {
        $searchTypeTravail = $this->typeTravailRepository->findOneBy(["libelle" => $typeTravail->getLibelle()]);
        if (!empty($searchTypeTravail) && $typeTravail->getId() != $searchTypeTravail->getId()) {
            throw new LogicException($this->translator->trans("error.type_work.libelle_exist"));
        }

        $this->em->persist($typeTravail);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_TYPEWORK,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_TYPEWORK]["edit"],
            $typeTravail
        );
    }

    /**
     * @param TypeTravail $typeTravail
     */
    public function delete(TypeTravail $typeTravail)
    {
        $this->em->remove($typeTravail);
        $this->em->flush();
    }

    /**
     * @param TypeTravail $typeTravail
     * @return float|int
     */
    public function getRealDuration(TypeTravail $typeTravail)
    {
        /** @var ChantierRepository $chantierRepository */
        $chantierRepository = $this->em->getRepository(Chantier::class);

        $chantiers = $chantierRepository->findByTypeTravailFinish($typeTravail);

        if (empty($chantiers)) {
            return 0;
        }

        $total = 0;
        /** @var Chantier $chantier */
        foreach($chantiers as $chantier) {
            $total += $chantier->getPrepaRealisee();
        }

        return $total / count($chantiers);
    }
}