<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgenceRepository")
 * @ORM\Table(name="new_agence")
 */
class Agence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="BO", mappedBy="agence")
     */
    private $listBo;

    /**
     * @ORM\ManyToOne(targetEntity="DR", inversedBy="agences")
     */
    private $dr;

    /**
     * Agence constructor.
     */
    public function __construct()
    {
        $this->listBo = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getListBo()
    {
        return $this->listBo;
    }

    /**
     * @param mixed $listBo
     */
    public function setListBo($listBo)
    {
        $this->listBo = $listBo;
    }

    /**
     * @param BO $bo
     */
    public function addBo(BO $bo)
    {
        $this->listBo->add($bo);
        $bo->setAgence($this);
    }

    /**
     * @param BO $bo
     */
    public function removeBo(BO $bo)
    {
        $this->listBo->removeElement($bo);
        $bo->setAgence(null);
    }

    /**
     * @return mixed
     */
    public function getDr()
    {
        return $this->dr;
    }

    /**
     * @param mixed $dr
     */
    public function setDr($dr)
    {
        $this->dr = $dr;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }
}
