<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommuneRepository")
 * @ORM\Table(name="new_commune")
 */
class Commune
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $insee;

    /**
     * @ORM\OneToMany(targetEntity="Chantier", mappedBy="commune")
     */
    private $chantiers;

    /**
     * @ORM\ManyToMany(targetEntity="BO", inversedBy="communes")
     */
    private $listBo;

    /**
     * Commune constructor.
     */
    public function __construct()
    {
        $this->chantiers = new ArrayCollection();
        $this->listBo = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getInsee()
    {
        return $this->insee;
    }

    /**
     * @param mixed $insee
     */
    public function setInsee($insee)
    {
        $this->insee = $insee;
    }

    /**
     * @return mixed
     */
    public function getChantiers()
    {
        return $this->chantiers;
    }

    /**
     * @param mixed $chantiers
     */
    public function setChantiers($chantiers)
    {
        $this->chantiers = $chantiers;
    }

    /**
     * @param Chantier $chantier
     */
    public function addChantier(Chantier $chantier)
    {
        if (!$this->chantiers->contains($chantier)) {
            $this->chantiers->add($chantier);
        }
    }

    /**
     * @param Chantier $chantier
     */
    public function removeChantier(Chantier $chantier)
    {
        if ($this->chantiers->contains($chantier)) {
            $this->chantiers->removeElement($chantier);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getListBo()
    {
        return $this->listBo;
    }

    /**
     * @param ArrayCollection $listBo
     */
    public function setListBo(ArrayCollection $listBo): void
    {
        $this->listBo = $listBo;
    }

    /**
     * @param BO $bo
     */
    public function addBo(BO $bo)
    {
        if (!$this->listBo->contains($bo)){
            $this->listBo->add($bo);
        }
    }

    /**
     * @param BO $bo
     */
    public function removeBo(BO $bo)
    {
        if ($this->listBo->contains($bo)) {
            $this->listBo->removeElement($bo);
        }
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->libelle . " (" . $this->insee . ")";
    }

    public function getLibelleComplet(){
    	return $this->getLibelle() . ' (' . $this->getInsee() . ')';
	}

}
