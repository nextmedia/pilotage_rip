<?php

namespace App\Controller;

use App\Entity\TypeTravail;
use App\Form\TypeTravailType;
use App\Repository\ChantierRepository;
use App\Repository\TypeTravailRepository;
use App\Service\TypeTravailService;
use App\Service\UtilsService;
use LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TypeTravailController
 * @package App\Controller
 * @Route("/travail", name="typework_")
 */
class TypeTravailController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param TypeTravailRepository $typeTravailRepository
     * @return Response
     */
    public function main(TypeTravailRepository $typeTravailRepository)
    {
        $travaux = $typeTravailRepository->findAll();

        return $this->render("travail/main.html.twig", [
            "travaux" => $travaux
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param TypeTravailService $typeTravailService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        TypeTravailService $typeTravailService,
        TranslatorInterface $translator
    ) {
        $typeTravail = new TypeTravail();

        $typeTravailForm = $this->createForm(TypeTravailType::class, $typeTravail);
        $typeTravailForm->handleRequest($request);

        if ($typeTravailForm->isSubmitted() && $typeTravailForm->isValid()) {
            try {
                $typeTravailService->init($typeTravail);
                $this->addFlash("success", $translator->trans("type_work.sentence.new_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
                return $this->redirectToRoute("typework_new");
            }

            return $this->redirectToRoute("typework_info", ["id" => $typeTravail->getId()]);
        }

        return $this->render("travail/info.html.twig", [
            "typeTravailForm" => $typeTravailForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param Request $request
     * @param TypeTravail $typeTravail
     * @param TypeTravailService $typeTravailService
     * @param TranslatorInterface $translator
     * @param ChantierRepository $chantierRepository
     * @return Response
     */
    public function info(
        Request $request,
        TypeTravail $typeTravail,
        TypeTravailService $typeTravailService,
        TranslatorInterface $translator,
        ChantierRepository $chantierRepository
    ) {
        $typeTravailForm = $this->createForm(TypeTravailType::class, $typeTravail);
        $typeTravailForm->handleRequest($request);

        if ($typeTravailForm->isSubmitted() && $typeTravailForm->isValid()) {
            try {
                $typeTravailService->edit($typeTravail);
                $this->addFlash("success", $translator->trans("utils.edit_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirectToRoute("typework_info", ["id" => $typeTravail->getId()]);
        }

        $chantiers = $chantierRepository->findBy(["typeTravail" => $typeTravail]);

        return $this->render("travail/info.html.twig", [
            "travail" => $typeTravail,
            "chantiers" => $chantiers,
            "typeTravailForm" => $typeTravailForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param TypeTravail $typeTravail
     * @param TypeTravailService $typeTravailService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        TypeTravail $typeTravail,
        TypeTravailService $typeTravailService,
        TranslatorInterface $translator
    ) {
        $typeTravailService->delete($typeTravail);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("typework_main");
    }

    /**
     * @Route("/{id}/duration", name="duration", options={"expose"=true})
     * @param TypeTravail $typeTravail
     * @param UtilsService $utilsService
     * @return JsonResponse
     */
    public function getDuration(TypeTravail $typeTravail, UtilsService $utilsService)
    {
        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);

        if (!empty($typeTravail->getDuration())) {
            $response["duration"] = $typeTravail->getDuration();
        } else {
            $response["duration"] = 0;
        }

        return new JsonResponse($response);
    }
}