<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Agence;
use App\Entity\DR;
use App\Repository\DRRepository;
use LogicException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class DRService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var DRRepository $DRRepository */
    private $DRRepository;
    /** @var Security $security */
    private $security;
    /** @var ActionService $actionService */
    private $actionService;

    /**
     * AgenceService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param DRRepository $DRRepository
     * @param Security $security
     * @param ActionService $actionService
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        DRRepository $DRRepository,
        Security $security,
        ActionService $actionService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->DRRepository = $DRRepository;
        $this->security = $security;
        $this->actionService = $actionService;
    }

    /**
     * @param DR $dr
     */
    public function init(DR $dr)
    {
        $searchDR = $this->DRRepository->findOneBy(["name" => $dr->getName()]);
        if (!empty($searchDR)) {
            throw new LogicException($this->translator->trans("error.dr.name_exist"));
        }

        if (!empty($dr->getAgences())) {
            /** @var Agence */
            foreach($dr->getAgences()->getValues() as $agence) {
                $dr->addAgence($agence);
            }
        }

        $this->em->persist($dr);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_DR,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_DR]["create"],
            $dr
        );
    }

    /**
     * @param DR $dr
     */
    public function edit(DR $dr)
    {
        $searchDR = $this->DRRepository->findOneBy(["name" => $dr->getName()]);
        if (!empty($searchDR) && $dr->getId() != $searchDR->getId()) {
            throw new LogicException($this->translator->trans("error.dr.name_exist"));
        }

        $agences = $this->em->getRepository(Agence::class)->findBy(["dr" => $dr->getId()]);

        $toRemove = array_udiff($agences, $dr->getAgences()->getValues(), [$this, "compareAgence"]);
        $toAdd = array_udiff( $dr->getAgences()->getValues(), $agences, [$this, "compareAgence"]);

        /** @var Agence $agence */
        foreach($toRemove as $agence) { $dr->removeAgence($agence); }
        /** @var Agence $agence */
        foreach($toAdd as $agence) { $dr->addAgence($agence); }

        $this->em->persist($dr);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_DR,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_DR]["edit"],
            $dr
        );
    }

    /**
     * Permet la comparaison entre 2 Agences dans le array_udiff
     * @param Agence $a
     * @param Agence $b
     * @return int
     */
    private function compareAgence(Agence $a, Agence $b)
    {
        if ($a->getId() < $b->getId()) {
            return -1;
        } elseif ($a->getId() > $b->getId()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @param DR $dr
     */
    public function delete(DR $dr)
    {
        $this->actionService->newAction(
            Action::TYPE_DR,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_DR]["remove"],
            $dr
        );

        $this->em->remove($dr);
        $this->em->flush();
    }
}