<?php

namespace App\Repository;

use App\Entity\CommentaireRevue;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method CommentaireRevue|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommentaireRevue|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommentaireRevue[]    findAll()
 * @method CommentaireRevue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireRevueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommentaireRevue::class);
    }
}
