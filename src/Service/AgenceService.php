<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\BO;
use LogicException;
use App\Entity\Agence;
use App\Repository\AgenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class AgenceService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var AgenceRepository $agenceRepository */
    private $agenceRepository;
    /** @var Security $security */
    private $security;
    /** @var ActionService $actionService */
    private $actionService;

    /**
     * AgenceService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param AgenceRepository $agenceRepository
     * @param Security $security
     * @param ActionService $actionService
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        AgenceRepository $agenceRepository,
        Security $security,
        ActionService $actionService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->agenceRepository = $agenceRepository;
        $this->security = $security;
        $this->actionService = $actionService;
    }

    /**
     * @param Agence $agence
     */
    public function init(Agence $agence)
    {
        $searchAgence = $this->agenceRepository->findOneBy(["name" => $agence->getName()]);
        if (!empty($searchAgence)) {
            throw new LogicException($this->translator->trans("error.agency.name_exist"));
        }

        if (!empty($agence->getListBo())) {
            /** @var BO $bo */
            foreach($agence->getListBo()->getValues() as $bo) {
                $agence->addBo($bo);
            }
        }

        $this->em->persist($agence);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_AGENCE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_AGENCE]["create"],
            $agence
        );
    }

    /**
     * @param Agence $agence
     */
    public function edit(Agence $agence)
    {
        $searchAgence = $this->agenceRepository->findOneBy(["name" => $agence->getName()]);
        if (!empty($searchAgence) && $agence->getId() != $searchAgence->getId()) {
            throw new LogicException($this->translator->trans("error.agency.name_exist"));
        }

        $listBo = $this->em->getRepository(BO::class)->findBy(["agence" => $agence->getId()]);

        $toRemove = array_udiff($listBo, $agence->getListBo()->getValues(), [$this, "compareBo"]);
        $toAdd = array_udiff($agence->getListBo()->getValues(), $listBo, [$this, "compareBo"]);

        /** @var BO $bo */
        foreach($toRemove as $bo) { $agence->removeBo($bo); }
        /** @var BO $bo */
        foreach($toAdd as $bo) { $agence->addBo($bo); }

        $this->actionService->newAction(
            Action::TYPE_AGENCE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_AGENCE]["edit"],
            $agence
        );

        $this->em->persist($agence);
        $this->em->flush();
    }

    /**
     * Permet la comparaison entre 2 BO dans le array_udiff
     * @param BO $a
     * @param BO $b
     * @return int
     */
    private function compareBo(BO $a, BO $b)
    {
        if ($a->getId() < $b->getId()) {
            return -1;
        } elseif ($a->getId() > $b->getId()) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @param Agence $agence
     */
    public function delete(Agence $agence)
    {
        /** @var BO $bo */
        foreach($agence->getListBo()->getValues() as $bo) {
            $agence->removeBo($bo);
        }

        $this->actionService->newAction(
            Action::TYPE_AGENCE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_AGENCE]["remove"],
            $agence
        );

        $this->em->remove($agence);
        $this->em->flush();
    }
}