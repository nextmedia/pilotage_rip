Pour rafraichir les routes, faire cette commande:
* php bin/console fos:js-routing:dump --format=json --target=assets/js/routing/fos_js_routes.json

Comment accèder à l'application:
* Faire un compte sur l'application avec le NNI
* Accès à l'application sur le serveur d'ENEDIS : https://suivi-rip.appheb.enedis-grdf.fr:19419/index.php

A l'installation sur la machine d'ENEDIS:
* Dans les routes (/assets/js/routing/fos_js_routes.json), ajouter "/index.php" dans "base_url"
* yarn encore prod et mettre les modification dans public