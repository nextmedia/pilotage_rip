<?php

namespace App\Controller;

use App\Entity\Chantier;
use App\Entity\User;
use App\Form\FilterType;
use App\Form\UploadFileType;
use App\Form\UserType;
use App\Service\ChantierService;
use App\Service\UserService;
use App\Repository\UserRepository;
use App\Repository\ChantierRepository;
use App\Service\UtilsService;
use DateTime;
use Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param UserRepository $userRepository
     * @param Request $request
     * @return Response
     */
    public function main(UserRepository $userRepository, Request $request)
    {
        $filter = $request->get("filter");
        if (!empty($filter)) {
            $users = $userRepository->findByRole($filter);
        } else {
            $users = $userRepository->findAll();
        }

        return $this->render("user/main.html.twig", [
            "users" => $users
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param User $user
     * @param Request $request
     * @param UserService $userService
     * @param ChantierRepository $chantierRepository
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function info(
        User $user,
        Request $request,
        UserService $userService,
        ChantierRepository $chantierRepository,
        TranslatorInterface $translator
    ) {
        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $userService->edit($user);

            $this->addFlash("success", $translator->trans("utils.edit_success"));

            return $this->redirectToRoute("user_info", ["id" => $user->getId()]);
        }

        $chantiers = $chantierRepository->findByPreparateur($user);

        return $this->render("user/info.html.twig", [
            "user" => $user,
            "chantiers" => $chantiers,
            "userForm" => $userForm->createView()
        ]);
    }

    /**
     * @Route("/review", name="rip_review")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserService $userService
     * @return Response
     */
    public function review(
        UserRepository $userRepository,
        Request $request,
        UserService $userService
    ) {
        $users = $userRepository->findByRole(User::ROLE_RIP);

        $formFilter = $this->createForm(FilterType::class);
        $formFilter->handleRequest($request);

        if ($formFilter->isSubmitted() && $formFilter->isValid()) {
            $datas = $formFilter->getData();

            $users = $userService->getUsersByFilter($datas["dr"], $datas["agence"], $datas["bo"], User::ROLE_RIP);
        }

        return $this->render("user/review.html.twig", [
            "users" => $users,
            "formFilter" => $formFilter->createView()
        ]);
    }

    /**
     * @Route("/review/{id}", name="rip_review_sites")
     * @param User $user
     * @param ChantierRepository $chantierRepository
     * @return Response
     */
    public function reviewSites(
        User $user,
        ChantierRepository $chantierRepository
    ) {
        $chantiers = $chantierRepository->findReview($user);

        return $this->render("chantier/main.html.twig", [
            "user" => $user,
            "chantiers" => $chantiers
        ]);
    }

    /**
     * @Route("/review/{id}/close", name="close_review", options={"expose"=true})
     * @param User $user
     * @param UserService $userService
     * @param UtilsService $utilsService
     * @param Request $request
     * @return JsonResponse
     */
    public function closeReview(
        User $user,
        UserService $userService,
        UtilsService $utilsService,
        Request $request
    ) {
        $commentaire = $request->get("commentaire");

        $userService->closeReview($user, $commentaire);

        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);

        return new JsonResponse($response);
    }

    /**
     * @Route("/getUsers", name="get_users", options={"expose"=true})
     * @param UserRepository $userRepository
     * @param UtilsService $utilsService
     * @param Request $request
     * @return JsonResponse
     */
    public function getUsers(
        UserRepository $userRepository,
        UtilsService $utilsService,
        Request $request
    ) {
        $id = $request->get("idBo");
        $users = [];

        $datas = empty($id) ? $userRepository->findAll() : $userRepository->findBy(["bo" => $id]);

        foreach($datas as $user) {
            $users[$user->getId()] = $user->getFirstname() . " " . $user->getLastname();
        }

        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);
        $response["users"] = $users;

        return new JsonResponse($response);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param UserService $userService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        UserService $userService,
        TranslatorInterface $translator
    ) {
        $user = new User();

        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $userService->init($user);

            $this->addFlash("success", $translator->trans("user.sentence.new_success"));

            return $this->redirectToRoute("user_info", ["id" => $user->getId()]);
        }

        return $this->render("user/info.html.twig", [
            "userForm" => $userForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param User $user
     * @param UserService $userService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        User $user,
        UserService $userService,
        TranslatorInterface $translator
    ) {
        $userService->delete($user);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("user_main");
    }

    /**
     * @Route("/importPlanning", name="import_planning")
     * @param Request $request
     * @param UserService $userService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function importPlanning(
        Request $request,
        UserService $userService,
        TranslatorInterface $translator
    ) {
        $form = $this->createForm(UploadFileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();

            try {
                foreach ($datas["fichier"] as $fichier) {
                    $userService->importPlanning($fichier);
                }
                $this->addFlash("success", $translator->trans("utils.import_success"));
            } catch (Exception $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirectToRoute("user_import_planning");
        }

        return $this->render("user/importPlanning.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/stats", name="stats")
     * @param User $user
     * @param ChantierService $chantierService
     * @return Response
     */
    public function stats(User $user, ChantierService $chantierService, ChantierRepository $chantierRepository)
    {
        /** @var User $connected */
        $connected = $this->getUser();

        if($connected->hasRole(User::ROLE_RIP) && $connected->getId() != $user->getId()) {
            throw new AccessDeniedException();
        }
        $start = new \DateTime();
        $end = date_modify(new \DateTime(), "+1 month");

        $open = count($chantierRepository->findByUserAndDate($user, $start, $end));
        $charge = $chantierService->calculCharge($user, $start, $end);

        $response[$user->getId()]["dispo"] = $charge["dispo"];
        $response[$user->getId()]["charge"] = $charge["charge"];
        $response[$user->getId()]["open"] = $open;

        return $this->render("user/statistiques.html.twig", [
            "users" => [$user],
            "datas" => $response
        ]);
    }

    /**
     * @Route("/{id}/getStats", name="get_stats", options={"expose"=true})
     * @param User $user
     * @param ChantierService $chantierService
     * @param Request $request
     * @param UtilsService $utilsService
     * @param ChantierRepository $chantierRepository
     * @return JsonResponse
     */
    public function getStats(
        User $user,
        ChantierService $chantierService,
        Request $request,
        UtilsService $utilsService,
        ChantierRepository $chantierRepository
    ) {
        $start = DateTime::createFromFormat('d/m/Y', $request->get("start"));
        $end = DateTime::createFromFormat('d/m/Y', $request->get("end"));

        $startClone = clone $start;
        $endClone = clone $end;

        $charge = $chantierService->calculCharge($user, $startClone, $endClone);
        $open = count($chantierRepository->findByUserAndDate($user, $start, $end));
        $closed = count($chantierRepository->findByUserAndDateFinished($user, $start, $end));

        $response = $utilsService->getJsonResponse(true, UtilsService::SUCCESS);
        $response["dispo"] = $charge["dispo"];
        $response["charge"] = $charge["charge"];
        $response["open"] = $open;
        $response["closed"] = $closed;

        return new JsonResponse($response);
    }
}