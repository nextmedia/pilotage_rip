<?php

namespace App\Service;

use App\Entity\Action;
use LogicException;
use App\Entity\Commune;
use App\Repository\CommuneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommuneService
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;
    /** @var CommuneRepository $communeRepository */
    private $communeRepository;
    /** @var Security $security */
    private $security;
    /** @var ActionService $actionService */
    private $actionService;

    /**
     * CommuneService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @param CommuneRepository $communeRepository
     * @param Security $security
     * @param ActionService $actionService
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        CommuneRepository $communeRepository,
        Security $security,
        ActionService $actionService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->communeRepository = $communeRepository;
        $this->security = $security;
        $this->actionService = $actionService;
    }

    /**
     * @param Commune $commune
     */
    public function init(Commune $commune)
    {
        $searchCommune = $this->communeRepository->findOneBy(["insee" => $commune->getInsee()]);
        if (!empty($searchCommune)) {
            throw new LogicException($this->translator->trans("error.municipality.insee_exist"));
        }

        $this->em->persist($commune);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_COMMUNE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_COMMUNE]["create"],
            $commune
        );
    }

    /**
     * @param Commune $commune
     */
    public function edit(Commune $commune)
    {
        $searchCommune = $this->communeRepository->findOneBy(["insee" => $commune->getInsee()]);
        if (!empty($searchCommune) && $commune->getId() != $searchCommune->getId()) {
            throw new LogicException($this->translator->trans("error.municipality.insee_exist"));
        }

        $this->em->persist($commune);
        $this->em->flush();

        $this->actionService->newAction(
            Action::TYPE_COMMUNE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_COMMUNE]["edit"],
            $commune
        );
    }

    /**
     * @param Commune $commune
     */
    public function delete(Commune $commune)
    {
        $this->actionService->newAction(
            Action::TYPE_COMMUNE,
            $this->security->getUser(),
            Action::ACTION[Action::TYPE_COMMUNE]["remove"],
            $commune
        );

        $this->em->remove($commune);
        $this->em->flush();
    }
}