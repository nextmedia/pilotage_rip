const routes = require('./routing/fos_js_routes.json');

import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

export {changeOption};

$(function() {
    filterReview();
    onLoadPage();
});

/**
 * Gère les filtres des BO lors des reviews
 */
function filterReview() {
    var $dr = $("#filter_dr"),
        $agence = $("#filter_agence"),
        $bo = $("#filter_bo");

    $dr.off("select2:select").on("select2:select", function(e) {
        var id = e.params.data.id;

        $.ajax({
            url : Routing.generate("app_filter_review",{"id" : id, "type" : "add_dr"}),
            type : "POST",
            dataType : "json",
            success : function(data){
                changeOption(data.agences, $agence);
                changeOption(data.listBo, $bo);
            }
        });
    });

    $dr.off("select2:unselect").on("select2:unselect", function() {
        $.ajax({
            url : Routing.generate("app_filter_review",{"type" : "remove_dr", "agence" : $agence.val()}),
            type : "POST",
            dataType : "json",
            success : function(data){
                changeOption(data.agences, $agence);
                changeOption(data.listBo, $bo);
            }
        });
    });

    $agence.off("select2:select").on("select2:select", function(e) {
        var id = e.params.data.id;

        $.ajax({
            url : Routing.generate("app_filter_review",{"id" : id, "type" : "add_agence"}),
            type : "POST",
            dataType : "json",
            success : function(data){
                changeOption(data.listBo, $bo);
                changeOption(data.agences, $agence);

                $agence.val(id).trigger("change");
                $dr.val(data.dr).trigger("change");

            }
        });
    });

    $agence.off("select2:unselect").on("select2:unselect", function() {
        $.ajax({
            url : Routing.generate("app_filter_review",{"type" : "remove_agence", "bo" : $bo.val(), "dr" : $dr.val()}),
            type : "POST",
            dataType : "json",
            success : function(data){
                var drId = $dr.val(),
                    boId = $bo.val();

                changeOption(data.listBo, $bo);
                if (drId) {
                    $dr.val(drId);
                }
                if (boId) {
                    $bo.val(boId);
                }
            }
        });
    });

    $bo.off("select2:select").on("select2:select", function(e) {
        var id = e.params.data.id;

        $.ajax({
            url : Routing.generate("app_filter_review",{"id" : id, "type" : "add_bo"}),
            type : "POST",
            dataType : "json",
            success : function(data){
                changeOption(data.listBo, $bo);
                changeOption(data.agences, $agence);

                $dr.val(data.dr).trigger("change");
                $agence.val(data.agence).trigger("change");
                $bo.val(id).trigger("change");
            }
        });
    });
}

/**
 * Au chargement de la page avec les filtres
 */
function onLoadPage() {
    var $filterDr = $("#filter_dr"),
        $filterAgence = $("#filter_agence"),
        $filterBo = $("#filter_bo");

    if ($filterDr.length > 0 && $filterAgence.length > 0 && $filterBo.length > 0) {
        if ($filterBo.val()) {
            var id = $filterBo.val();
            $.ajax({
                url : Routing.generate("app_filter_review",{"id" : id, "type" : "add_bo"}),
                type : "POST",
                dataType : "json",
                success : function(data){
                    changeOption(data.listBo, $filterBo);
                    changeOption(data.agences, $filterAgence);

                    $filterDr.val(data.dr).trigger("change");
                    $filterAgence.val(data.agence).trigger("change");
                    $filterBo.val(id).trigger("change");
                }
            });
        } else if ($filterAgence.val()) {
            var id = $filterAgence.val();
            $.ajax({
                url : Routing.generate("app_filter_review",{"id" : id, "type" : "add_agence"}),
                type : "POST",
                dataType : "json",
                success : function(data){
                    changeOption(data.listBo, $filterBo);
                    changeOption(data.agences, $filterAgence);

                    $filterAgence.val(id).trigger("change");
                    $filterDr.val(data.dr).trigger("change");
                }
            });
        } else if ($filterDr.val()) {
            var id = $filterDr.val();
            $.ajax({
                url : Routing.generate("app_filter_review",{"id" : id, "type" : "add_dr"}),
                type : "POST",
                dataType : "json",
                success : function(data){
                    changeOption(data.agences, $filterAgence);
                    changeOption(data.listBo, $filterBo);
                }
            });
        }
    }
}

/**
 * Actualise les données dans les selects
 * @param items
 * @param $select
 */
function changeOption(items, $select) {
    $select.html("");
    for (var key in items) {
        $select.append(new Option(items[key], key, false, false));
    }
    $select.val("").trigger("change");
}