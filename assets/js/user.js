const routes = require('./routing/fos_js_routes.json'),
    bootbox = require( './bootbox.all.min.js');

import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

import Chart from '../../node_modules/chart.js/dist/chart';
import {activeSelect2, activateDatepicker, bootboxConfirm, openLogbook} from "./main";
import {addCommentary, selectTypeTravail, changeTime, closeSite, onValidate, openSite, showCharge, clearDate} from "./chantier";
export {openSiteReview};

$(function() {
    closeReview();
    openSiteReview();
    changeDateCharge();
    imports();
});

/**
 * Cloture une revue
 */
function closeReview() {
    var $btn = $(".btn-close-review");

    $btn.off("click").on("click", function() {
        var that = $(this),
            id = that.data("id");

        bootbox.prompt({
            animate: false,
            title: "Voulez-vous cloturer cette revue ?",
            inputType: "textarea",
            backdrop: true,
            buttons: {
                confirm: {
                    label: "Cloturer"
                },
                cancel: {
                    label: "Annuler"
                }
            },
            callback: function (result) {
                var url = Routing.generate("user_close_review", {"id" : id, "commentaire" : result});
                $.ajax({
                    url : url,
                    type : "POST",
                    dataType : "json",
                    success : function(){
                        that.removeClass("btn-warning");
                        that.addClass("btn-success");
                        that.html("<i class='fa fa-check'></i>");
                        that.removeClass("btn-close-review");
                    }
                });
            }
        });
    });
}

/**
 * Ouvre les informations d'un chantier lors des revues
 */
function openSiteReview() {
    var $btn = $(".btn-open-site"),
        $close = $(".close-site"),
        $firstBox = $("#FirstBox"),
        $secondBox = $("#SecondBox");

    $close.off("click").on("click", function() {
        $firstBox.removeClass("hide");
        $secondBox.addClass("hide");
    });

    $btn.off("click").on("click", function() {
        var that = $(this),
            url = Routing.generate("site_details", {"id" : that.data("id")}),
            $body = $secondBox.find(".box-header");

        $body.html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
        $firstBox.addClass("hide");
        $secondBox.removeClass("hide");

        $.ajax({
            url : url,
            type : "POST",
            dataType : "html",
            success : function(data){
                $body.html(data);
                activeSelect2();
                activateDatepicker();
                bootboxConfirm();
                addCommentary();
                selectTypeTravail();
                changeTime();
                closeSite();
                openSite();
                onValidate();
                openLogbook();
                showCharge();
                clearDate();
            }
        });
    });
}

/**
 * Au changement de créneau pour le calcul de la charge
 */
function changeDateCharge() {
    var $btn = $(".select-time"),
        userId = $("input[data-user]").data("user");

    $btn.off("click").on("click", function() {
        var that = $(this),
            $selected = $(".select-time, .btn-primary"),
            start = that.data("start"),
            end = that.data("end");

        $.ajax({
            url : Routing.generate("user_get_stats", {"id" : userId, "start" : start, "end" : end}),
            type : "POST",
            dataType : "json",
            success : function(data){
                var porcent = 0;

                if (data.dispo !== 0) { porcent = data.charge / data.dispo * 100; }
                $('td[data-info="open"]').html(data.open);
                $('td[data-info="dispo"]').html(data.dispo);
                $('td[data-info="charge"]').html(data.charge);
                $('td[data-info="pourcentage"]').html(porcent.toFixed(1));

                $selected.removeClass("btn-primary");
                $selected.addClass("btn-default");

                that.addClass("btn-primary");
                that.removeClass("btn-default");

                window.chartPie.data.datasets[0].data[0] = data.open;
                window.chartPie.data.datasets[0].data[1] = data.closed;
                window.chartPie.update()
            }
        });
    });
}

/**
 * Au clic pour lancer un import
 */
function imports() {
    $(".btn-import").on("click", function() {
        var input = $("#upload_file_fichier");

        if (input[0].files.length > 0) {
            $("#Spinner").removeClass("d-none");
        }
    })
}