<?php

namespace App\Repository;

use App\Entity\DR;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method DR|null find($id, $lockMode = null, $lockVersion = null)
 * @method DR|null findOneBy(array $criteria, array $orderBy = null)
 * @method DR[]    findAll()
 * @method DR[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DRRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DR::class);
    }
}
