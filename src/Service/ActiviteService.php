<?php

namespace App\Service;

use App\Entity\Activite;
use Doctrine\ORM\EntityManagerInterface;

class ActiviteService
{
    /** @var EntityManagerInterface $em */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __contruct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Activite $activite
     */
    public function init(Activite $activite)
    {
        $this->em->persist($activite);
        $this->em->flush();
    }
}