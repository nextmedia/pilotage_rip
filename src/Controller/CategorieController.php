<?php

namespace App\Controller;

use App\Service\UtilsService;
use LogicException;
use App\Entity\Categorie;
use App\Form\CategorieType;
use App\Service\CategorieService;
use App\Repository\ChantierRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CategorieController
 * @package App\Controller
 * @Route("/categorie", name="category_")
 */
class CategorieController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param CategorieRepository $categorieRepository
     * @return Response
     */
    public function main(CategorieRepository $categorieRepository)
    {
        $categories = $categorieRepository->findAll();

        return $this->render("categorie/main.html.twig", [
            "categories" => $categories
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param CategorieService $categorieService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        CategorieService $categorieService,
        TranslatorInterface $translator
    ) {
        $categorie = new Categorie();

        $categorieForm = $this->createForm(CategorieType::class, $categorie);
        $categorieForm->handleRequest($request);

        if ($categorieForm->isSubmitted() && $categorieForm->isValid()) {
            try {
                $categorieService->init($categorie);
                $this->addFlash("success", $translator->trans("category.sentence.new_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
                return $this->redirectToRoute("category_new");
            }

            return $this->redirectToRoute("category_info", ["id" => $categorie->getId()]);
        }

        return $this->render("categorie/info.html.twig", [
            "categorieForm" => $categorieForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param Request $request
     * @param Categorie $categorie
     * @param CategorieService $categorieService
     * @param ChantierRepository $chantierRepository
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function info(
        Request $request,
        Categorie $categorie,
        CategorieService $categorieService,
        ChantierRepository $chantierRepository,
        TranslatorInterface $translator
    ) {
        $categorieForm = $this->createForm(CategorieType::class, $categorie);
        $categorieForm->handleRequest($request);

        if ($categorieForm->isSubmitted() && $categorieForm->isValid()) {
            try {
                $categorieService->edit($categorie);
                $this->addFlash("success", $translator->trans("utils.edit_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirectToRoute("category_info", ["id" => $categorie->getId()]);
        }

        $chantiers = $chantierRepository->findBy(["categorie" => $categorie->getId()]);

        return $this->render("categorie/info.html.twig", [
            "categorie" => $categorie,
            "chantiers" => $chantiers,
            "categorieForm" => $categorieForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param Categorie $categorie
     * @param CategorieService $categorieService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        Categorie $categorie,
        CategorieService $categorieService,
        TranslatorInterface $translator
    ) {
        $categorieService->delete($categorie);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("category_main");
    }
}