<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActiviteRepository")
 * @ORM\Table(name="new_activite")
 */
class Activite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    private $libelle;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isPrepa;

    /**
     * @ORM\OneToMany(targetEntity="Planning", mappedBy="activite")
     */
    private $plannings;

    /**
     * Activite constructor.
     */
    public function __construct()
    {
        $this->isPrepa = false;
        $this->plannings = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getIsPrepa()
    {
        return $this->isPrepa;
    }

    /**
     * @param mixed $isPrepa
     */
    public function setIsPrepa($isPrepa)
    {
        $this->isPrepa = $isPrepa;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlannings()
    {
        return $this->plannings;
    }

    /**
     * @param ArrayCollection $plannings
     */
    public function setPlannings(ArrayCollection $plannings)
    {
        $this->plannings = $plannings;
    }

    /**
     * @param Planning $planning
     */
    public function addPlanning(Planning $planning)
    {
        $this->plannings->add($planning);
    }

    /**
     * @param Planning $planning
     */
    public function removePlanning(Planning $planning)
    {
        $this->plannings->removeElement($planning);
    }
}
