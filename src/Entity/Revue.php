<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RevueRepository")
 * @ORM\Table(name="new_revue")
 */
class Revue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="revues")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="revuesCreated")
     */
    private $createdFor;

    /**
     * @ORM\ManyToMany(targetEntity="Chantier", inversedBy="revues")
     */
    private $chantiers;

    /**
     * @ORM\OneToOne(targetEntity="CommentaireRevue")
     */
    private $commentaire;

    /**
     * Revue constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->chantiers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getCreatedFor()
    {
        return $this->createdFor;
    }

    /**
     * @param mixed $createdFor
     */
    public function setCreatedFor($createdFor)
    {
        $this->createdFor = $createdFor;
    }

    /**
     * @return ArrayCollection
     */
    public function getChantiers()
    {
        return $this->chantiers;
    }

    /**
     * @param $chantiers
     */
    public function setChantiers($chantiers)
    {
        $this->chantiers = $chantiers;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "toto";
    }
}
