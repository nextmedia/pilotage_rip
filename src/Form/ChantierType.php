<?php

namespace App\Form;

use App\Entity\BO;
use App\Entity\Categorie;
use App\Entity\Chantier;
use App\Entity\Commune;
use App\Entity\TypeTravail;
use App\Entity\User;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ChantierType extends AbstractType
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * CommuneType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class, [
                "label" => $this->translator->trans("site.number"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('codeAffaire', TextType::class, [
                "label" => $this->translator->trans("site.code_affaire"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('etat', TextType::class, [
                "label" => $this->translator->trans("site.state"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('nomDemandeur', TextType::class, [
                "label" => $this->translator->trans("site.nom_demandeur"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('entiteDemandeur', TextType::class, [
                "label" => $this->translator->trans("site.entity_demandeur"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('rue', TextType::class, [
                "label" => $this->translator->trans("site.street"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('complement', TextType::class, [
                "label" => $this->translator->trans("site.complement"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('intitule', TextType::class, [
                "label" => $this->translator->trans("site.body"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('prepaRetenue', NumberType::class, [
                "label" => $this->translator->trans("site.prepa_retenue"),
                "required" => false,
                'html5' => true,
                'data' => $options['data']->getId() ? $options['data']->getPrepaRetenue() : "0",
                "attr" => [
                    "min" => 0,
                    "step" => 1,
                    "class" => "form-control textforms"
                ]
            ])
            ->add('prepaRealisee', NumberType::class, [
                "label" => $this->translator->trans("site.prepa_realisee"),
                "required" => false,
                'html5' => true,
                'data' => $options['data']->getId() ? $options['data']->getPrepaRealisee() : "0",
                "attr" => [
                    "min" => 0,
                    "step" => 1,
                    "class" => "form-control textforms"
                ]
            ])
            ->add('priorite', NumberType::class, [
                "label" => $this->translator->trans("site.priority"),
                "required" => false,
                'html5' => true,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('dateDebutReal', DateType::class, [
                "label" => $this->translator->trans("site.date.start_real"),
                "required" => false,
                "format" => "dd/MM/yyyy",
                "widget" => "single_text",
                "attr" => [
                    "class" => "form-control textforms datepicker",
                    "autocomplete" => "off"
                ]
            ])
            ->add('dateDebutPrepa', DateType::class, [
                "label" => $this->translator->trans("site.date.start_prepa"),
                "required" => false,
                "format" => "dd/MM/yyyy",
                "widget" => "single_text",
                "attr" => [
                    "class" => "form-control textforms datepicker",
                    "autocomplete" => "off"
                ]
            ])
            ->add('dateFinReal', DateType::class, [
                "label" => $this->translator->trans("site.date.end_real"),
                "required" => false,
                "format" => "dd/MM/yyyy",
                "widget" => "single_text",
                "attr" => [
                    "class" => "form-control textforms datepicker",
                    "autocomplete" => "off"
                ]
            ])
            ->add('dateFinPrepa', DateType::class, [
                "label" => $this->translator->trans("site.date.end_prepa"),
                "required" => false,
                "format" => "dd/MM/yyyy",
                "widget" => "single_text",
                "attr" => [
                    "class" => "form-control textforms datepicker",
                    "autocomplete" => "off"
                ]
            ])
            ->add('entitePrepa', EntityType::class, [
                "label" => $this->translator->trans("site.entity_prepa"),
                "class" => BO::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('entiteReal', EntityType::class, [
                "class" => BO::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('nomCA', TextType::class, [
                "label" => $this->translator->trans("site.name_ca"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('type', TextType::class, [
                "label" => $this->translator->trans("site.type"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('categorie', EntityType::class, [
                "label" => $this->translator->trans("category.libelle", ["%count%" => 1]),
                "class" => Categorie::class,
                "choice_label" => "libelle",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('typeTravail', EntityType::class, [
                "label" => $this->translator->trans("type_work.libelle"),
                "class" => TypeTravail::class,
                "choice_label" => "libelle",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('preparateur', EntityType::class, [
                "label" => $this->translator->trans("site.preparator"),
                "class" => User::class,
                "choice_label" => function ($user) {
                    return $user->getIdentity();
                },
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('responsable', EntityType::class, [
                "label" => $this->translator->trans("site.manager"),
                "class" => User::class,
                "choice_label" => function ($user) {
                    return $user->getIdentity();
                },
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('commune', EntityType::class, [
                "label" => $this->translator->trans("municipality.libelle", ["%count%" => 1]),
                "class" => Commune::class,
                "empty_data" => "toto",
                "choice_label" => function (Commune $municipality) {
                    return $municipality->getLibelle() . " (" . $municipality->getInsee() . ")";
                },
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Chantier::class
        ]);
    }
}
