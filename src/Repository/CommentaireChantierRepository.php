<?php

namespace App\Repository;

use App\Entity\Chantier;
use App\Entity\CommentaireChantier;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method CommentaireChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommentaireChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommentaireChantier[]    findAll()
 * @method CommentaireChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommentaireChantier::class);
    }

    /**
     * @param Chantier $chantier
     * @return int|mixed|string
     */
    public function findLast(Chantier $chantier)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('commentaire')
            ->from(CommentaireChantier::class, 'commentaire')
            ->leftJoin("commentaire.chantier", "chantier")
            ->where("chantier.id = :id")
            ->orderBy('commentaire.createdAt', 'DESC')
            ->setParameter("id", $chantier->getId())
        ;

        return $qb->getQuery()->getResult();
    }
}
