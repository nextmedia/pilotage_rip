<?php

namespace App\Repository;

use App\Entity\Agence;
use App\Entity\DR;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Agence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Agence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Agence[]    findAll()
 * @method Agence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Agence::class);
    }

    /**
     * @param DR $dr
     * @return QueryBuilder
     */
    public function findAvailablesAgence(DR $dr)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("a")
            ->from(Agence::class, "a")
            ->leftJoin("a.dr", "d")
            ->where("d.id = :id")
            ->orWhere("d.id IS NULL")
            ->setParameter("id", $dr->getId())
        ;

        return $query;
    }
}
