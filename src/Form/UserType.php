<?php

namespace App\Form;

use App\Entity\BO;
use App\Entity\User;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserType extends AbstractType
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * CommuneType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                "label" => $this->translator->trans("user.firstname"),
                "required" => true,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('lastname', TextType::class, [
                "label" => $this->translator->trans("user.lastname"),
                "required" => true,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('nni', TextType::class, [
                "label" => "NNI",
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('email', EmailType::class, [
                "label" => $this->translator->trans("user.email"),
                "required" => false,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('coefficient', NumberType::class, [
                "label" => $this->translator->trans("user.ratio"),
                'html5' => true,
                "required" => true,
                "attr" => [
                    "min" => 0,
                    "scale" => 2,
                    "step" => 0.05,
                    "class" => "form-control textforms",
                    "autocomplete" => "off"
                ]
            ])
            ->add('dateLastReview', DateType::class, [
                "label" => $this->translator->trans("user.date_last_review"),
                "required" => false,
                "format" => "dd/MM/yyyy",
                "widget" => "single_text",
                "attr" => [
                    "class" => "form-control textforms datepicker",
                    "autocomplete" => "off"
                ]
            ])
            ->add('bo', EntityType::class, [
                "label" => $this->translator->trans("bo.libelle"),
                "class" => BO::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('roles', ChoiceType::class, [
                "label" => $this->translator->trans("user.role", ["%count%" => 1]),
                "required" => true,
                "multiple" => true,
                'choices'  => User::ROLES,
                "attr" => [
                    "class" => "textforms form-control select2"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => $this->translator->trans("utils.validate"),
                "attr" => [
                    "class" => "btn btn-success btn-sm"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
