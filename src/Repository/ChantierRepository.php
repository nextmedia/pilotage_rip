<?php

namespace App\Repository;

use App\Entity\BO;
use App\Entity\TypeTravail;
use App\Entity\User;
use App\Entity\Agence;
use App\Entity\Chantier;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Chantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chantier[]    findAll()
 * @method Chantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chantier::class);
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function findByUser(User $user)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("c")
            ->from(Chantier::class, "c")
            ->leftJoin("c.preparateur", "p")
            ->leftJoin("c.responsable", "r")
            ->where("p.id = :id")
            ->orWhere("r.id = :id")
            ->setParameter("id", $user->getId())
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function findByPreparateur(User $user)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("c")
            ->from(Chantier::class, "c")
            ->leftJoin("c.preparateur", "p")
            ->where("p.id = :id")
            ->setParameter("id", $user->getId())
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param BO $bo
     * @return int|mixed|string
     */
    public function findByBo(BO $bo)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("c")
            ->from(Chantier::class, "c")
            ->where("c.entitePrepa = :id")
            ->orWhere("c.entiteReal = :id")
            ->setParameter("id", $bo->getId())
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function findReview(User $user)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('chantier')
            ->from(Chantier::class, 'chantier')
            ->leftjoin("chantier.preparateur", "user")
            ->where('user.id = :id')
            ->andWhere("chantier.etat != '".Chantier::STATE_ANNULE."'")
            ->andWhere("chantier.etat != '".Chantier::STATE_CLOTURE."'")
            ->andWhere("chantier.etat != '".Chantier::STATE_REFUSE."'")
//            ->andWhere("chantier.dateCloture IS NULL")
            ->setParameter('id', $user->getId())
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param bool $status
     * @return int|mixed|string
     */
    public function findByStatus(User $user, bool $status)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('chantier')
            ->from(Chantier::class, 'chantier')
            ->where('chantier.preparateur = :user')
            ->setParameter('user', $user)
        ;

        if ($status) {
            $qb->andWhere("chantier.dateCloture IS NULL");
        } else {
            $qb->andWhere("chantier.dateCloture IS NOT NULL");
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return int|mixed|string
     */
    public function findUnassigned()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('chantier')
            ->from(Chantier::class, 'chantier')
            ->where('chantier.preparateur IS NULL')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param $start
     * @param $end
     * @return int|mixed|string
     */
    public function findByUserAndDate(User $user, $start, $end)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("chantier")
            ->from(Chantier::class, "chantier")
            ->leftJoin("chantier.preparateur", "user")
            ->where("user.id = :userId")
            ->andWhere("chantier.dateCloture IS NULL")
//            ->andWhere("chantier.dateDebutPrepa < :end")
            ->andWhere("chantier.dateFinPrepa < :end")
            ->andWhere("chantier.dateFinPrepa > :start")
            ->setParameter("userId", $user->getId())
            ->setParameter("end", $end)
            ->setParameter("start", $start)
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param TypeTravail $typeTravail
     * @return int|mixed|string
     */
    public function findByTypeTravailFinish(TypeTravail $typeTravail)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("chantier")
            ->from(Chantier::class, "chantier")
            ->leftJoin("chantier.typeTravail", "typeTravail")
            ->where("typeTravail.id = :typeTravailID")
            ->andWhere("chantier.prepaRetenue IS NOT NULL")
            ->setParameter("typeTravailID", $typeTravail->getId())
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function findSitesLastReview(User $user)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('c')
            ->from(Chantier::class, "c")
            ->leftJoin("c.preparateur", "u")
            ->where("u.id = :userId")
            ->andWhere("c.dateCloture > u.dateLastReview")
            ->setParameter('userId', $user->getId())
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param $start
     * @param $end
     * @return int|mixed|string
     */
    public function findByUserAndDateFinished(User $user, $start, $end)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("chantier")
            ->from(Chantier::class, "chantier")
            ->leftJoin("chantier.preparateur", "user")
            ->where("user.id = :userId")
            ->andWhere("chantier.dateCloture IS NOT NULL")
            ->andWhere("chantier.dateDebutPrepa < :end")
            ->andWhere("chantier.dateFinPrepa > :start")
            ->setParameter("userId", $user->getId())
            ->setParameter("end", $end)
            ->setParameter("start", $start)
        ;

        return $query->getQuery()->getResult();
    }
}
