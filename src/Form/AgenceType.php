<?php

namespace App\Form;

use App\Entity\BO;
use App\Entity\Agence;
use App\Entity\DR;
use App\Repository\BORepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AgenceType extends AbstractType
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * CommuneType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $agence = $options["data"];

        $builder
            ->add('name', TextType::class, [
                "label" => $this->translator->trans("agency.libelle", ["%count%" => 1]),
                "required" => true,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('dr', EntityType::class, [
                "label" => $this->translator->trans("dr.libelle"),
                "class" => DR::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => false,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add('listBo', EntityType::class, [
                "label" => $this->translator->trans("bo.libelle"),
                "class" => BO::class,
                "query_builder" => function (BORepository $boRepository) use ($agence) {
                    return $boRepository->findAvailablesBo($agence);
                },
                "choice_label" => "name",
                "required" => false,
                "multiple" => true,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control select2"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => $this->translator->trans("utils.validate"),
                "attr" => [
                    "class" => "btn btn-success btn-sm"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Agence::class,
        ]);
    }
}
