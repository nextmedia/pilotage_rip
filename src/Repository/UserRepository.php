<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     * @param UserInterface $user
     * @param string $newEncodedPassword
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @param $role
     * @return int|mixed|string
     */
    public function findByRole(string $role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"'.$role.'"%')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $dr
     * @param $agence
     * @param $bo
     * @param null $role
     * @return int|mixed|string
     */
    public function findByFilter($dr, $agence, $bo, $role = null)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('user')
            ->from(User::class, 'user')
            ->leftJoin("user.bo", "bo")
            ->leftJoin("bo.agence", "agence")
            ->leftJoin("agence.dr", "dr")
            ->where("user.id IS NOT NULL")
        ;

        if (!empty($dr)) {
            $qb
                ->andWhere("dr = :dr_id")
                ->setParameter('dr_id', $dr->getId())
            ;
        }

        if (!empty($agence)) {
            $qb
                ->andWhere("agence = :agence_id")
                ->setParameter('agence_id', $agence->getId())
            ;
        }

        if (!empty($bo)) {
            $qb
                ->andWhere("bo = :bo_id")
                ->setParameter('bo_id', $bo->getId())
            ;
        }

        if (!empty($role)) {
            $qb
                ->andWhere('user.roles LIKE :roles')
                ->setParameter('roles', '%"'.$role.'"%');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $firstname
     * @param string $lastname
     * @return int|mixed|string
     */
    public function findOneByIdentity(string $firstname, string $lastname)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('user')
            ->from(User::class, 'user')
            ->where('user.firstname = :firstname')
            ->andWhere('user.lastname = :lastname')
            ->setParameter('firstname', $firstname)
            ->setParameter('lastname', $lastname)
        ;

        if (sizeof($qb->getQuery()->getResult()) === 0) {
            return null;
        }

        return $qb->getQuery()->getResult()[0];
    }
}
