<?php

namespace App\Repository;

use App\Entity\Planning;
use App\Entity\User;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Planning|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planning|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planning[]    findAll()
 * @method Planning[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Planning::class);
    }

    /**
     * @param $user
     * @param $start
     * @param $end
     * @return QueryBuilder
     */
    public function findExistantPlanning($user, $start, $end)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("planning")
            ->from(Planning::class, "planning")
            ->leftJoin("planning.user", "user")
            ->where("user.id = :userId")
            ->andWhere("planning.start = :start")
            ->andWhere("planning.end = :end")
            ->setParameter("userId", $user->getId())
            ->setParameter("start", $start)
            ->setParameter("end", $end)
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param $start
     * @param $end
     * @param $bool
     * @return int|mixed|string
     */
    public function findByUserAndDate(User $user, $start, $end, $bool)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select("planning")
            ->from(Planning::class, "planning")
            ->leftJoin("planning.user", "user")
            ->leftJoin("planning.activite", "activite")
            ->where("user.id = :userId")
            ->andWhere("activite.isPrepa = :bool")
            ->andWhere("planning.start <= :end")
            ->andWhere("planning.end >= :start")
            ->setParameter("userId", $user->getId())
            ->setParameter("bool", $bool)
            ->setParameter("start", $start)
            ->setParameter("end", $end)
        ;

        return $query->getQuery()->getResult();
    }
}
