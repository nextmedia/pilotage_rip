<?php

namespace App\Controller;

use App\Entity\DR;
use App\Form\DRType;
use App\Repository\DRRepository;
use App\Service\DRService;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DRController
 * @package App\Controller
 * @Route("/dr", name="dr_")
 */
class DRController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param DRRepository $DRRepository
     * @return Response
     */
    public function main(DRRepository $DRRepository)
    {
        $listDr = $DRRepository->findAll();

        return $this->render("dr/main.html.twig", [
            "listDr" => $listDr
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param DRService $drService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        DRService $drService,
        TranslatorInterface $translator
    ) {
        $dr = new DR();

        $drForm = $this->createForm(DRType::class, $dr);
        $drForm->handleRequest($request);

        if ($drForm->isSubmitted() && $drForm->isValid()) {
            try {
                $drService->init($dr);
                $this->addFlash("success", $translator->trans("dr.sentence.new_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
                return $this->redirectToRoute("dr_new");
            }

            return $this->redirectToRoute("dr_info", ["id" => $dr->getId()]);
        }

        return $this->render("dr/info.html.twig", [
            "drForm" => $drForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param DR $dr
     * @param Request $request
     * @param DRService $DRService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function info(
        DR $dr,
        Request $request,
        DRService $DRService,
        TranslatorInterface $translator
    ) {
        $drForm = $this->createForm(DRType::class, $dr);
        $drForm->handleRequest($request);

        if ($drForm->isSubmitted() && $drForm->isValid()) {
            try {
                $DRService->edit($dr);
                $this->addFlash("success", $translator->trans("utils.edit_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirectToRoute("dr_info", ["id" => $dr->getId()]);
        }

        $agences = $dr->getAgences();

        return $this->render("dr/info.html.twig", [
            "dr" => $dr,
            "agences" => $agences,
            "drForm" => $drForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param DR $dr
     * @param DRService $drService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        DR $dr,
        DRService $drService,
        TranslatorInterface $translator
    ) {
        $drService->delete($dr);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("dr_main");
    }
}