<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Agence;
use App\Entity\BO;
use App\Entity\DR;
use App\Repository\ActionRepository;
use App\Repository\AgenceRepository;
use App\Repository\BORepository;
use App\Repository\DRRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UtilsService
{
    CONST SUCCESS = "SUCCESS";

    /** @var EntityManagerInterface $em */
    private $em;
    /** @var TranslatorInterface $translator */
    private $translator;

    /**
     * AgenceService constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     */
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ) {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * @param bool $status
     * @param string $message
     * @return array
     */
    public function getJsonResponse(bool $status, string $message) {
        return [
            "status" => $status,
            "message" => $message
        ];
    }

    /**
     * @param $id
     * @param $type
     * @param $dr
     * @return array
     */
    public function getDataFilter($id, $type, $dr)
    {
        if ($type === "add_dr") {
            return $this->filterAddDr($id);
        } elseif ($type === "add_agence") {
            return $this->filterAddAgence($id);
        }elseif ($type === "add_bo") {
            return $this->filterAddBo($id);
        } elseif ($type === "remove_dr") {
            return $this->filterRemoveDr();
        } elseif ($type === "remove_agence") {
            return $this->filterRemoveAgence($dr);
        }

        return null;
    }

    /**
     * @param int $id
     * @return array
     */
    private function filterAddDr(int $id)
    {
        /** @var DRRepository $drRepository */
        $drRepository = $this->em->getRepository(DR::class);
        /** @var AgenceRepository $agenceRepository */
        $agenceRepository = $this->em->getRepository(Agence::class);
        /** @var BORepository $boRepository */
        $boRepository = $this->em->getRepository(BO::class);

        /** @var DR $dr */
        $dr = $drRepository->find($id);
        $agences = [];
        $listBo = [];

        foreach($agenceRepository->findBy(["dr" => $id]) as $agence) {
            $agences[$agence->getId()] = $agence->getName();
        }
        foreach($boRepository->findByDR($dr) as $bo) {
            $listBo[$bo->getId()] = $bo->getName();
        }

        return [
            "status" => true,
            "message" => UtilsService::SUCCESS,
            "agences" => $agences,
            "listBo" => $listBo
        ];
    }

    /**
     * @param int $id
     * @return array
     */
    private function filterAddAgence(int $id)
    {
        /** @var BORepository $boRepository */
        $boRepository = $this->em->getRepository(BO::class);
        /** @var AgenceRepository $agenceRepository */
        $agenceRepository = $this->em->getRepository(Agence::class);

        $agence = $agenceRepository->find($id);
        $dr = $agence->getDr();
        $listBo = [];
        $agences = [];

        foreach($boRepository->findBy(["agence" => $id]) as $bo) {
            $listBo[$bo->getId()] = $bo->getName();
        }
        foreach($agenceRepository->findBy(["dr" => $dr->getId()]) as $agence) {
            $agences[$agence->getId()] = $agence->getName();
        }

        return [
            "status" => true,
            "message" => UtilsService::SUCCESS,
            "dr" => $dr->getId(),
            "listBo" => $listBo,
            "agences" => $agences
        ];
    }

    /**
     * @param int $id
     * @return array
     */
    private function filterAddBo(int $id)
    {
        /** @var BORepository $boRepository */
        $boRepository = $this->em->getRepository(BO::class);

        /** @var BO $bo */
        $bo = $boRepository->find($id);
        /** @var Agence $agence */
        $agence = $bo->getAgence();
        /** @var DR $dr */
        $dr = $agence->getDr();

        $listBo = [];
        foreach($boRepository->findBy(["agence" => $agence->getId()]) as $bo) {
            $listBo[$bo->getId()] = $bo->getName();
        }

        $agences = [];
        foreach($dr->getAgences() as $data) {
            $agences[$data->getId()] = $data->getName();
        }

        return [
            "status" => true,
            "message" => UtilsService::SUCCESS,
            "dr" => $dr->getId(),
            "agence" => $agence->getId(),
            "agences" => $agences,
            "listBo" => $listBo
        ];
    }

    /**
     * @return array
     */
    private function filterRemoveDr()
    {
        /** @var AgenceRepository $agenceRepository */
        $agenceRepository = $this->em->getRepository(Agence::class);
        /** @var BORepository $boRepository */
        $boRepository = $this->em->getRepository(BO::class);

        $agences = [];
        $listBo = [];

        foreach($agenceRepository->findAll() as $agence) {
            $agences[$agence->getId()] = $agence->getName();
        }

        foreach($boRepository->findAll() as $bo) {
            $listBo[$bo->getId()] = $bo->getName();
        }

        return [
            "status" => true,
            "message" => UtilsService::SUCCESS,
            "agences" => $agences,
            "listBo" => $listBo
        ];
    }

    /**
     * @param $drId
     * @return array
     */
    private function filterRemoveAgence($drId)
    {
        /** @var DRRepository $drRepository */
        $drRepository = $this->em->getRepository(DR::class);
        /** @var BORepository $boRepository */
        $boRepository = $this->em->getRepository(BO::class);

        $listBo = [];

        if (empty($drId)) {
            foreach($boRepository->findAll() as $bo) {
                $listBo[$bo->getId()] = $bo->getName();
            }
        } else {
            $dr = $drRepository->find($drId);
            foreach($boRepository->findByDR($dr) as $bo) {
                $listBo[$bo->getId()] = $bo->getName();
            }
        }

        return [
            "status" => true,
            "message" => UtilsService::SUCCESS,
            "listBo" => $listBo
        ];
    }

    /**
     * @param $object
     * @param $type
     * @param null $actions
     * @return array
     */
    public function getActionsByObjectAndDate($object, $type, $actions = null)
    {
        /** @var ActionRepository $repoAction */
        $repoAction = $this->em->getRepository(Action::class);
        if(empty($actions)){
            $actions = [];
            $actions["count"] = 0;
            $actions["actions"] = [];
        }
        $results = $repoAction->findBy(["idSource" => $object->getId(), "type" => $type]);

        if(!empty($results)){
            $count = count($results);

            foreach ($results as $action){
                $actions["actions"][$action->getCreatedAt()->format("Ymd-His")][] = $action;
            }

            // Nombre total d'actions
            $actions["count"] = $actions["count"] + $count;

            krsort($actions["actions"]);
        }

        return $actions;
    }
}