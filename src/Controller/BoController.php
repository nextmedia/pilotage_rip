<?php

namespace App\Controller;

use App\Entity\BO;
use App\Entity\Commune;
use LogicException;
use App\Form\BoType;
use App\Service\BoService;
use App\Service\UtilsService;
use App\Repository\BORepository;
use App\Repository\ChantierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class BoController
 * @package App\Controller
 * @Route("/bo", name="bo_")
 */
class BoController extends AbstractController
{
    /**
     * @Route("", name="main")
     * @param BORepository $boRepository
     * @return Response
     */
    public function main(BORepository $boRepository)
    {
        $listBo = $boRepository->findAll();

        return $this->render("bo/main.html.twig", [
            "listBo" => $listBo
        ]);
    }

    /**
     * @Route("/new", name="new")
     * @param Request $request
     * @param BoService $boService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function new(
        Request $request,
        BoService $boService,
        TranslatorInterface $translator
    ) {
        $bo = new BO();

        $boForm = $this->createForm(BoType::class, $bo);
        $boForm->handleRequest($request);

        if ($boForm->isSubmitted() && $boForm->isValid()) {
            try {
                $boService->init($bo);
                $this->addFlash("success", $translator->trans("bo.sentence.new_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
                return $this->redirectToRoute("bo_new");
            }

            return $this->redirectToRoute("bo_info", ["id" => $bo->getId()]);
        }

        return $this->render("bo/info.html.twig", [
            "boForm" => $boForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/info", name="info")
     * @param Request $request
     * @param BO $bo
     * @param BoService $boService
     * @param ChantierRepository $chantierRepository
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function info(
        Request $request,
        BO $bo,
        BoService $boService,
        ChantierRepository $chantierRepository,
        TranslatorInterface $translator
    ) {
        $boForm = $this->createForm(BoType::class, $bo);
        $boForm->handleRequest($request);

        if ($boForm->isSubmitted() && $boForm->isValid()) {
            try {
                $boService->edit($bo);
                $this->addFlash("success", $translator->trans("utils.edit_success"));
            } catch (LogicException $e) {
                $this->addFlash("danger", $e->getMessage());
            }

            return $this->redirectToRoute("bo_info", ["id" => $bo->getId()]);
        }

        $chantiers = $chantierRepository->findByBo($bo);

        return $this->render("bo/info.html.twig", [
            "bo" => $bo,
            "chantiers" => $chantiers,
            "boForm" => $boForm->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete")
     * @param BO $bo
     * @param BoService $boService
     * @param TranslatorInterface $translator
     * @return RedirectResponse
     */
    public function delete(
        BO $bo,
        BoService $boService,
        TranslatorInterface $translator
    ) {
        $boService->delete($bo);

        $this->addFlash("success", $translator->trans("utils.delete_success"));

        return $this->redirectToRoute("bo_main");
    }
}