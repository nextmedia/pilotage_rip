<?php

namespace App\Form;

use App\Entity\BO;
use App\Entity\Commune;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CommuneType extends AbstractType
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * CommuneType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', TextType::class, [
                "label" => $this->translator->trans("utils.libelle"),
                "required" => true,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('insee', TextType::class, [
                "label" => $this->translator->trans("municipality.insee_code"),
                "required" => true,
                "attr" => [
                    "class" => "form-control textforms"
                ]
            ])
            ->add('listBo', EntityType::class, [
                "label" => $this->translator->trans("bo.libelle"),
                "class" => BO::class,
                "choice_label" => "name",
                "required" => false,
                "multiple" => true,
                "expanded" => false,
                "attr" => [
                    "class" => "form-control textforms select2"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => $this->translator->trans("utils.validate"),
                "attr" => [
                    "class" => "btn btn-success btn-sm"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Commune::class,
        ]);
    }
}
