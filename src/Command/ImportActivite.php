<?php


namespace App\Command;

use App\Entity\Activite;
use App\Repository\ActiviteRepository;
use App\Service\ActiviteService;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportActivite extends Command
{
    protected static $defaultName = 'import:activite';

    /** @var EntityManagerInterface $em */
    private $em;
    /** @var KernelInterface $kernel */
    private $kernel;
    /** @var ActiviteService $activiteService */
    private $activiteService;

    /**
     * ImportSuiviSociety constructor.
     * @param EntityManagerInterface $em
     * @param KernelInterface $kernel
     * @param ActiviteService $activiteService
     */
    public function __construct(
        EntityManagerInterface $em,
        KernelInterface $kernel,
        ActiviteService $activiteService
    ) {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->activiteService = $activiteService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Import des activites')

            ->addArgument('name', InputArgument::REQUIRED, 'nom du fichier')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Import des activites.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Import en cours ...");

        try {
            $this->import($input);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return -1;
        }

        $output->writeln("Terminé !");

        return 0;
    }

    /**
     * @param InputInterface $input
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function import(InputInterface $input)
    {
        $path = $this->kernel->getProjectDir() . "/uploads";

        $name = $input->getArgument("name");
        $fileName = "/".$name.".csv";

        if (!file_exists($path.$fileName)){
            throw new \PhpOffice\PhpSpreadsheet\Exception($path.$fileName);
        }

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');
        $reader->setReadDataOnly(TRUE);

        $spreadsheet = $reader->load($path.$fileName);

        $worksheet = $spreadsheet->getActiveSheet();

        /** @var ActiviteRepository $activiteRepository */
        $activiteRepository = $this->em->getRepository(Activite::class);

        foreach ($worksheet->getRowIterator() as $row) {
            if($row->getRowIndex() === 1) continue;

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);

            $arrayLine = [];
            foreach ($cellIterator as $cell) {
                $arrayLine[$cell->getColumn()] = $cell->getValue();
            }

            if (empty($activiteRepository->findBy(["libelle" => $arrayLine["J"]]))) {
                $activite = new Activite();
                $activite->setLibelle($arrayLine["J"]);
                $this->em->persist($activite);
                $this->em->flush();
            }
        }
    }
}