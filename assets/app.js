import './app.scss';

require('admin-lte/bower_components/jquery/dist/jquery');
require('admin-lte/bower_components/jquery-ui/jquery-ui');
require('admin-lte/bower_components/jquery-ui/ui/i18n/datepicker-fr');
require('admin-lte/bower_components/bootstrap/dist/js/bootstrap');
require('admin-lte/bower_components/select2/dist/js/select2');
require('admin-lte/bower_components/datatables.net/js/jquery.dataTables');
require('admin-lte/bower_components/chart.js/Chart');
require('admin-lte/dist/js/adminlte');
require('admin-lte/bower_components/bootstrap-daterangepicker');

require( './js/main');
require( './js/agence');
require( './js/chantier');
require( './js/filtre');
require( './js/user');
require( './js/switchButton');