<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRevueRepository")
 * @ORM\Table(name="new_commentaire_revue")
 */
class CommentaireRevue extends Commentaire
{
    /**
     * @ORM\OneToOne(targetEntity="Revue")
     */
    private $revue;

    /**
     * @return mixed
     */
    public function getRevue()
    {
        return $this->revue;
    }

    /**
     * @param mixed $revue
     */
    public function setRevue($revue)
    {
        $this->revue = $revue;
    }
}
