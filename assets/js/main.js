const bootbox = require( './bootbox.all.min.js'),
    routes = require('./routing/fos_js_routes.json');

import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import {onSwitchButton} from "./switchButton";
import {selectPrepaCharge} from "./chantier";

Routing.setRoutingData(routes);

export {activeSelect2, activateDatepicker, bootboxConfirm, openLogbook, getMessageFlash, initDatatables};
import {openSiteReview} from "./user";

$(function() {
    initDatatables();
    initTables();
    activeSelect2();
    activateDatepicker();
    bootboxConfirm();
    divRedirect();
    openLogbook();
    initDateRangePicker();
});

/**
 * Activation des datatables
 */
function initDatatables() {
    var $datatables = $(".datatable");

    $datatables.each(function() {
        var that = $(this),
            orderArray = [];

        if ($.fn.DataTable.isDataTable(that)) {
            return;
        }

        if(that.find("th.sorting_asc").length > 0){
            orderArray = [ $(that.find("th.sorting_asc")[0]).index(), 'asc'];
        }else if($datatables.find("th.sorting_desc").length > 0){
            orderArray = [ $(that.find("th.sorting_desc")[0]).index(), 'desc'];
        }else{
            orderArray = [ 0, 'asc'];
        }

        that.DataTable({
            statesave: true,
            language: {
                searchPlaceholder: "Recherche Globale",
                search: "",
                lengthMenu: "Afficher _MENU_ entrées",
                info: "Montre les entrées entre _START_ et _END_ | max : _TOTAL_",
                infoEmpty: "Montre les entrées entre 0 et 0 | max : 0",
                emptyTable: "Aucune information dans cette table",
                zeroRecords: "Aucun résultat obtenu",
                paginate: {
                    "first": "Premier",
                    "last": "Dernier",
                    "next": "Suivant",
                    "previous": "Précedent",
                    "infoEmpty": "Pas d'informations",
                    "processing": "Recherche...",
                    "loadingRecords": "Chargement..."
                }
            },
            order: [orderArray]
        });
    })

    $datatables.on( 'search.dt', function () {
        divRedirect();
        bootboxConfirm();
        onSwitchButton();
        selectPrepaCharge();
        openSiteReview();
    });

    $datatables.on( 'draw.dt', function () {
        divRedirect();
        bootboxConfirm();
        onSwitchButton();
        selectPrepaCharge();
        openSiteReview();
    });
}

/**
 * Activation des tables
 */
function initTables() {
    var $tables = $(".table-default");

    $tables.DataTable({
        statesave: true,
        ordering: false,
        searching: false,
        paginate: false,
        language: {
            searchPlaceholder: "Recherche Globale",
            search: "",
            lengthMenu: "Afficher _MENU_ entrées",
            info: "Montre les entrées entre _START_ et _END_ | max : _TOTAL_",
            infoEmpty: "Montre les entrées entre 0 et 0 | max : 0",
            emptyTable: "Aucune information dans cette table",
            zeroRecords: "Aucun résultat obtenu",
            paginate: {
                "first": "Premier",
                "last": "Dernier",
                "next": "Suivant",
                "previous": "Précedent",
                "infoEmpty": "Pas d'informations",
                "processing": "Recherche...",
                "loadingRecords": "Chargement..."
            }
        }
    });
}

/**
 * Activation des select2
 */
function activeSelect2() {
    $(".select2").select2({
        placeholder: "Aucun(e)",
        allowClear: true,
        width: '100%',
        "language": {
            "noResults": function(){
                return "Aucun résultat";
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    });
}

/**
 * Activation des datepickers
 */
function activateDatepicker() {
    var $datepickers = $(".datepicker");

    $datepickers.datepicker({ dateFormat: 'dd/mm/yy'  });
}

/**
 * Active les bootbox de confirmation
 */
function bootboxConfirm() {
    var $buttons = $(".bootbox-confirm");

    $buttons.off("click").on("click", function() {
       var that = $(this),
           message = that.data("message"),
           href = that.data("href"),
           button = that.data("button"),
           classes = that.data("classes");

       bootbox.confirm({
           animate: false,
           message: message,
           backdrop: true,
           buttons: {
               confirm: {
                   label: button,
                   className: classes
               },
               cancel: {
                   label: "Annuler",
                   className: "btn-sm btn-primary"
               }
           },
           callback: function (result) {
               if (result) {
                   window.location = href;
               }
           }
       });
    });
}

/**
 * Redirige au click
 */
function divRedirect() {
    var $redirect = $(".redirect");

    $redirect.off("click").on("click", function() {
       var that = $(this);

        window.location = that.data("href");
    });
}

/**
 * Ouvre l'historique dans un modal
 */
function openLogbook() {
    var $btn = $(".open-logbook");

    $btn.off("click").on("click", function() {
       var that = $(this),
           id = that.data("id"),
           objectClass = that.data("object_class"),
           type = that.data("type");

       var dialog = bootbox.dialog({
           message: '<p><i class="fa fa-spin fa-spinner"></i></p>'
       });

       dialog.init(function(){
           var url = Routing.generate("app_logbook", {"idSource" : id, "objectClass" : objectClass, "type" : type}),
               $body = dialog.find('.bootbox-body');

           $.ajax({
               url : url,
               type : "POST",
               dataType : "html",
               success : function(data){
                   $body.html("");
                   $body.append(data);
               }
           });
       });
    });
}

/**
 * Créer un message à afficher
 * @param type
 * @param message
 * @returns {string}
 */
function getMessageFlash(type, message) {
    return '<div class="alert alert-'+type+'"><button type="button" class="close" data-dismiss="alert" aria-label="Fermer"><span aria-hidden="true">&times;</span></button><p>'+message+'</p></div>';
}

/**
 * Initialise les daterangepickers
 */
function initDateRangePicker() {
    $(".daterangepicker").daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Valider",
            "cancelLabel": "Annuler",
            "fromLabel": "De",
            "toLabel": "à",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Dim",
                "Lun",
                "Mar",
                "Mer",
                "Jeu",
                "Ven",
                "Sam"
            ],
            "monthNames": [
                "Janvier",
                "Février",
                "Mars",
                "Avril",
                "Mai",
                "Juin",
                "Juillet",
                "Août",
                "Septembre",
                "Octobre",
                "Novembre",
                "Décembre"
            ],
            "firstDay": 1
        }
    });
}